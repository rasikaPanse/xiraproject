import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import firebase from 'firebase';

import { HttpClientModule } from '@angular/common/http'

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { MenuPage } from '../pages/menu/menu';
import { MenuPageModule } from '../pages/menu/menu.module'
import { UserPage } from '../pages/user/user';
import { UserPageModule } from '../pages/user/user.module';
import { LoginPage } from '../pages/login/login'; 
import { UserSecondPageModule } from '../pages/user-second/user-second.module';
// import { UserSecondPage }
import { AdminAssignTaskPage } from '../pages/admin-assign-task/admin-assign-task';
import { AdminAssignTaskPageModule } from '../pages/admin-assign-task/admin-assign-task.module'
// import { AddProjectPageModule } '../pages/add-project/add-project.module';
import { AddProjectPage } from '../pages/add-project/add-project';
import { AddProjectPageModule } from '../pages/add-project/add-project.module';
import { AdminLoginPageModule } from '../pages/admin-login/admin-login.module';
import { AdminLoginPage } from '../pages/admin-login/admin-login';
import { UserSecondPage } from '../pages/user-second/user-second';
import { UserServicesProvider } from '../providers/user-service/user-services';
import { AdminMainPageModule } from '../pages/admin-main/admin-main.module';
import { AdminMainPage } from '../pages/admin-main/admin-main';
import { AdminRegistrationPageModule } from '../pages/admin-registration/admin-registration.module';
import { AdminRegistrationPage } from '../pages/admin-registration/admin-registration';
import { AssignTaskPageModule } from '../pages/assign-task/assign-task.module';
import { AssignTaskPage } from '../pages/assign-task/assign-task';
import { CompletedTaskPageModule } from '../pages/completed-task/completed-task.module';
import { CompletedTaskPage } from '../pages/completed-task/completed-task';
import { PendingTaskPageModule } from '../pages/pending-task/pending-task.module';
import { PendingTaskPage } from '../pages/pending-task/pending-task';
import { ReviewTaskPageModule } from '../pages/review-task/review-task.module';
import { ReviewTaskPage } from '../pages/review-task/review-task';
import { TaskDifferentChoicePageModule} from '../pages/task-different-choice/task-different-choice.module';
import { TaskDifferentChoicePage } from '../pages/task-different-choice/task-different-choice';
import { UserRegistrationPageModule } from '../pages/user-registration/user-registration.module';
import { UserRegistrationPage } from '../pages/user-registration/user-registration';
import { UserTypeChoicePageModule } from '../pages/user-type-choice/user-type-choice.module';
import { UserTypeChoicePage } from '../pages/user-type-choice/user-type-choice';
import { UserprofilePageModule } from '../pages/userprofile/userprofile.module';
import { UserprofilePage } from '../pages/userprofile/userprofile';
import { AnimationService, AnimatesDirective } from 'css-animator';
import { AddTaskPage } from '../pages/add-task/add-task';
import { AddTaskPageModule } from '../pages/add-task/add-task.module';
import { ServicesPageModule } from '../pages/services/services.module';
import { ServicesPage } from '../pages/services/services';
import {IntlModule} from 'ng2-intl';
import {Ng2TelInputModule} from 'ng2-tel-input';
import { OneSignal } from '@ionic-native/onesignal';
import { AutosizeDirective} from '../directives/autosize/autosize';
import { AddDesignationPageModule } from '../pages/add-designation/add-designation.module';
import { AddDesignationPage } from '../pages/add-designation/add-designation';
import { AddProjectCategoryPageModule } from '../pages/add-project-category/add-project-category.module';
import { AddProjectCategoryPage } from '../pages/add-project-category/add-project-category';
// import { ProjectWiseReportPageModule } "../pages/project-wise-report/project-wise-report.module"
import { ProjectWiseReportPage } from '../pages/project-wise-report/project-wise-report';
import { ProjectWiseReportPageModule } from '../pages/project-wise-report/project-wise-report.module';
import { DetailedTaskPageModule } from '../pages/detailed-task/detailed-task.module';
import { DetailedTaskPage } from '../pages/detailed-task/detailed-task';
import { UserTimingDetailsPageModule } from '../pages/user-timing-details/user-timing-details.module';
import { UserTimingDetailsPage } from '../pages/user-timing-details/user-timing-details';
import { TimeformatePipe } from '../pipes/timeformate/timeformate';
import { PipesModule } from '../pipes/pipes.module';
import { ServiceEditPageModule } from '../pages/service-edit/service-edit.module';
import { ServiceEditPage } from '../pages/service-edit/service-edit';

export const config = {
  apiKey: "AIzaSyDfaTYOqZYhSHDVU0Sd94GukO3HCr03Ets",
  authDomain: "xirainfotechtracker.firebaseapp.com",
  databaseURL: "https://xirainfotechtracker.firebaseio.com",
  projectId: "xirainfotechtracker",
  storageBucket: "xirainfotechtracker.appspot.com",
  messagingSenderId: "445164844908"
};
firebase.initializeApp(config);
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    // UserSecondPage,
    AnimatesDirective,
    AutosizeDirective,
    
    // UserPage,
    // MenuPage
    
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    MenuPageModule,
    UserPageModule,
    UserSecondPageModule,
    AddProjectPageModule,
    AdminAssignTaskPageModule,
    AdminLoginPageModule,
    AdminMainPageModule,
    AdminRegistrationPageModule,
    AssignTaskPageModule,
    CompletedTaskPageModule,
    PendingTaskPageModule,
    ReviewTaskPageModule,
    TaskDifferentChoicePageModule,
    UserRegistrationPageModule,
    UserTypeChoicePageModule,
    UserprofilePageModule,
    AddDesignationPageModule,
    ProjectWiseReportPageModule,
    AddProjectCategoryPageModule,
    AddTaskPageModule,
    DetailedTaskPageModule,
    ServicesPageModule,
    UserTimingDetailsPageModule,
    PipesModule,
    ServiceEditPageModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    UserSecondPage,
    UserPage,
    MenuPage,
    AddProjectPage,
    AdminAssignTaskPage,
    AdminLoginPage,
    AdminMainPage,
    AdminRegistrationPage,
    AssignTaskPage,
    CompletedTaskPage,
    PendingTaskPage,
    ReviewTaskPage,
    TaskDifferentChoicePage,
    UserRegistrationPage,
    UserTypeChoicePage,
    UserprofilePage,
    AddDesignationPage,
    ProjectWiseReportPage,
    AddProjectCategoryPage,
    AddTaskPage,
    ServiceEditPage,
    DetailedTaskPage,
    UserTimingDetailsPage,
    ServicesPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserServicesProvider,
    AnimationService,
    OneSignal,
    
    //https://medium.com/appseed-io/how-to-integrate-onesignal-push-notifications-into-an-ionic-3-application-eb2fdc3e6176
    //https://onesignal.com/apps/70db32a5-ef1f-420c-8f0d-7d3902622353?restartEditAppFlow=true
  ]
})
export class AppModule {}
