import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AlertController,App } from 'ionic-angular';

import { HomePage } from '../pages/home/home';
// import { LoginPage } from '../pages/login/login';
import { OneSignal, OSNotificationPayload } from '@ionic-native/onesignal';
import { isCordovaAvailable } from '../common/is-cordova-available';
import { oneSignalAppId, sender_id } from '../config';
import { AdminMainPage } from '../pages/admin-main/admin-main';
import { TaskDifferentChoicePage } from '../pages/task-different-choice/task-different-choice';
import { UserTypeChoicePage } from '../pages/user-type-choice/user-type-choice';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  // rootPage:any = HomePage;
  // rootPage:any = UserTypeChoicePage;
  rootPage:any;
  isSignIn:any;
  UserType:any;
  public alertShown:boolean = false;

  constructor(public app:App,public platform: Platform,private oneSignal: OneSignal,public alertCtrl:AlertController, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // this language will be used as a fallback when a translation isn't found in the current language
     
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      if (isCordovaAvailable()){
        this.oneSignal.startInit(oneSignalAppId, sender_id);
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
        this.oneSignal.handleNotificationReceived().subscribe(data => this.onPushReceived(data.payload));
        this.oneSignal.handleNotificationOpened().subscribe(data => this.onPushOpened(data.notification.payload));
        this.oneSignal.endInit();
      }
      splashScreen.hide();
      this.isSignIn = localStorage.getItem("user");
      this.UserType = localStorage.getItem("type");

      if (this.isSignIn == 'signIn') {
        // alert("12345");
        if(this.UserType=="admin"){
          this.rootPage = 'MenuPage';
        }
        if(this.UserType=="user"){
          // this.rootPage =TabsPage
          this.rootPage= 'MenuPage'
        }

      }
      else {
        // alert("123");
        this.rootPage = UserTypeChoicePage;
       
      }

      platform.registerBackButtonAction(() => {
 
        let nav = app.getActiveNavs()[0];
        let activeView = nav.getActive();                
        console.log("active view",activeView.name);
        
        if(activeView.id === "AdminMainPage") {
          
              console.log("123");
                const alert = this.alertCtrl.create({
                    title: 'App termination',
                    message: 'Do you want to close the app?',
                    buttons: [{
                        text: 'Cancel',
                        role: 'cancel',
                        handler: () => {
                            console.log('Application exit prevented!');
                        }
                    },{
                        text: 'Close App',
                        handler: () => {
                            this.platform.exitApp(); // Close this application
                        }
                    }]
                });
                alert.present();
            
        }
        if(activeView.id === "AddProjectPage") {
          // alert("1");
            if (nav.canGoBack()){ //Can we go back?                                               
              // alert("alert");
                nav.pop();
            } else {
              // alert("123");
                nav.setRoot('AdminMainPage');
            }
        }
        if(activeView.id === "ReviewTaskPage") {
          // alert("1");
            if (nav.canGoBack()){ //Can we go back?                                               
              // alert("alert");
                nav.pop();
            } else {
              // alert("123");
                nav.setRoot('AdminMainPage');
            }
        }
        if(activeView.id === "AddProjectCategoryPage") {
          // alert("1");
            if (nav.canGoBack()){ //Can we go back?                                               
              // alert("alert");
                nav.pop();
            } else {
              // alert("123");
                nav.setRoot('AdminMainPage');
            }
        }
        if(activeView.id === "AddDesignationPage") {
          // alert("1");
            if (nav.canGoBack()){ //Can we go back?                                               
              // alert("alert");
                nav.pop();
            } else {
              // alert("123");
                nav.setRoot('AdminMainPage');
            }
        }
        if(activeView.id === "UserPage") {
          // alert("1");
            if (nav.canGoBack()){ //Can we go back?                                               
              // alert("alert");
                nav.pop();
            } else {
              // alert("123");
                nav.setRoot('AdminMainPage');
            }
        }
        if(activeView.id === "ProjectWiseReportPage") {
          // alert("1");
            if (nav.canGoBack()){ //Can we go back?                                               
              // alert("alert");
                nav.pop();
            } else {
              // alert("123");
                nav.setRoot('AdminMainPage');
            }
        }
        if(activeView.id === "UserSecondPage") {
          // alert("1");
            if (nav.canGoBack()){ //Can we go back?                                               
              // alert("alert");
                nav.pop();
            } else {
              // alert("123");
                nav.setRoot('AdminMainPage');
            }
        }
        if(activeView.name === "UserprofilePage") {
          // alert("1");
            if (nav.canGoBack()){ //Can we go back?                                               
              // alert("alert");
                nav.pop();
            } else {
              // alert("123");
                nav.setRoot('AdminMainPage');
            }
        }
        if(activeView.name === "PendingTaskPage") {
          // alert("1");
            if (nav.canGoBack()){ //Can we go back?                                               
              // alert("alert");
                nav.pop();
            } else {
              // alert("123");
                nav.setRoot('TaskDifferentChoicePage');
            }
        }
        if(activeView.name === "TaskDifferentChoicePage") {
          // alert("1");
            if (nav.canGoBack()){ //Can we go back?                                               
              // alert("alert");
                nav.pop();
            } else {
              console.log("123");
                const alert = this.alertCtrl.create({
                    title: 'App termination',
                    message: 'Do you want to close the app?',
                    buttons: [{
                        text: 'Cancel',
                        role: 'cancel',
                        handler: () => {
                            console.log('Application exit prevented!');
                        }
                    },{
                        text: 'Close App',
                        handler: () => {
                            this.platform.exitApp(); // Close this application
                        }
                    }]
                });
                alert.present();
            }
        }
        if(activeView.name === "DetailedTaskPage") {
          // alert("1");
            if (nav.canGoBack()){ //Can we go back?                                               
              // alert("alert");
                nav.pop();
            } else {
              // alert("123");
                nav.setRoot('TaskDifferentChoicePage');
            }
        }
        if(activeView.name === "ServicesPage") {
          // alert("1");
            if (nav.canGoBack()){ //Can we go back?                                               
              // alert("alert");
                nav.pop();
            } else {
              // alert("123");
                nav.setRoot('AdminMainPage');
            }
        }
        if(activeView.name === "ServiceEditPage") {
          // alert("1");
            if (nav.canGoBack()){ //Can we go back?                                               
              // alert("alert");
                nav.pop();
            } else {
              // alert("123");
                nav.setRoot('AdminMainPage');
            }
        }
        if(activeView.name === "AdminAssignTaskPage") {
          // alert("1");
            if (nav.canGoBack()){ //Can we go back?                                               
              // alert("alert");
                nav.pop();
            } else {
              // alert("123");
                nav.setRoot('TaskDifferentChoicePage');
            }
        }
        if(activeView.name === "CompletedTaskPage") {
          // alert("1");
            if (nav.canGoBack()){ //Can we go back?                                               
              // alert("alert");
                nav.pop();
            } else {
              // alert("123");
                nav.setRoot('TaskDifferentChoicePage');
            }
        
          }
          if(activeView.name === "DoneTaskPage") {
            // alert("1");
              if (nav.canGoBack()){ //Can we go back?                                               
                // alert("alert");
                  nav.pop();
              } else {
                // alert("123");
                  nav.setRoot('TaskDifferentChoicePage');
              }
          
            }
            if(activeView.name === "UserTimingDetailsPage") {
              // alert("1");
                if (nav.canGoBack()){ //Can we go back?                                               
                  // alert("alert");
                    nav.pop();
                } else {
                  // alert("123");
                    nav.setRoot('TaskDifferentChoicePage');
                }
            
              }
            if(activeView.name === "UserTypeChoicePage") {
          
              console.log("123");
                const alert = this.alertCtrl.create({
                    title: 'App termination',
                    message: 'Do you want to close the app?',
                    buttons: [{
                        text: 'Cancel',
                        role: 'cancel',
                        handler: () => {
                            console.log('Application exit prevented!');
                        }
                    },{
                        text: 'Close App',
                        handler: () => {
                            this.platform.exitApp(); // Close this application
                        }
                    }]
                });
                alert.present();
            
        }
    });
      // platform.registerBackButtonAction(() => {
      //   if (this.alertShown==false) {
      //     this.presentConfirm();  
      //   }
      // }, 0)
    });
  }

  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Confirm Exit',
      message: 'Do you want Exit?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
            this.alertShown=false;
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log('Yes clicked');
            this.platform.exitApp();
          }
        }
      ]
    });
     alert.present().then(()=>{
      this.alertShown=true;
    });
  }

  private onPushReceived(payload: OSNotificationPayload) {
    alert('Push recevied:' + payload.body);
  }
  
  private onPushOpened(payload: OSNotificationPayload) {
    alert('Push opened: ' + payload.body);
  }
}

