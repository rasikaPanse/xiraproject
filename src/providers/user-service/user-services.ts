import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import firebase from 'firebase';
import { Http } from '@angular/http';
import { HttpClientModule } from '@angular/common/http'
import 'rxjs/add/operator/map';
/*
  Generated class for the UserServicesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserServicesProvider {
  public fireAuth:any;
  public userProfile:any;
  public userId:any;
  public projectList:any;
  public companyName:any;
  public userType:any;
  constructor(public http: HttpClient) {
    console.log('Hello UserServicesProvider Provider');
    this.companyName="XiraInfotech"
    console.log("companyName",this.companyName);
    this.fireAuth = firebase.auth();
  }

  loginWithEmailAndPassword(email: string,password: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.fireAuth
        .signInWithEmailAndPassword(email, password)
        .then((val: any) => {
          // console.log("valsign", val);
          resolve(val);
        })
        .catch((error: any) => {
          reject(error);
        });
    });
  }

  registerWithEmailAndPassword(email: string, password: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.fireAuth.
        createUserWithEmailAndPassword(email, password)
        .then((val: any) => {
          console.log("valsign", val);
          resolve(val);
        })
        .catch((error: any) => {
          reject(error);
        })
    });
  }

  getUserOnAdminSide(){
    // alert("in p")
    // this.companyName=localStorage.getItem('companyName');

    this.userProfile = firebase.database().ref(this.companyName).child('UserProfile');

    const ref = this.userProfile
    return  new Promise((resolve, reject) => {
      ref.on('value', (resp,err) => {
        if(resp){
          let UserList = snapshotToArray(resp);
          // let temparr = [];
          // for (var key in Catdata) {
          //   temparr.push(Catdata[key]);
          // }
          console.log("tem", UserList);
          resolve(UserList);
        }
        else if(err){
          reject(err);
        }
       
      })
      
    })
  }

  getAdmin(email:string){
    // alert("in p")
    // this.companyName=localStorage.getItem('companyName');

    this.userProfile = firebase.database().ref('XiraInfotech').child('Admin').orderByChild('email').equalTo(email);

    const ref = this.userProfile
    return  new Promise((resolve, reject) => {
      ref.on('value', (resp,err) => {
        if(resp){
          let UserList = snapshotToArray(resp);
          // let temparr = [];
          // for (var key in Catdata) {
          //   temparr.push(Catdata[key]);
          // }
          console.log("tem", UserList);
          resolve(UserList);
        }
        else if(err){
          reject(err);
        }
       
      })
      
    })
  }

  getService(){
    // alert("in p")
    // this.companyName=localStorage.getItem('companyName');

    this.userProfile = firebase.database().ref(this.companyName).child('ProjectCategory');

    const ref = this.userProfile
    return  new Promise((resolve, reject) => {
      ref.on('value', (resp,err) => {
        if(resp){
          let UserList = snapshotToArray(resp);
          // let temparr = [];
          // for (var key in Catdata) {
          //   temparr.push(Catdata[key]);
          // }
          console.log("tem", UserList);
          resolve(UserList);
        }
        else if(err){
          reject(err);
        }
       
      })
      
    })
  }


  getUserOnAdminSideByStatus(status:string){
    this.userProfile = firebase.database().ref('XiraInfotech').child('UserProfile').orderByChild('userStatus').equalTo(status);

    const ref = this.userProfile
    return  new Promise((resolve, reject) => {
      ref.on('value', (resp,err) => {
        if(resp){
          let UserList = snapshotToArray(resp);
          // let temparr = [];
          // for (var key in Catdata) {
          //   temparr.push(Catdata[key]);
          // }
          console.log("tem", UserList);
          resolve(UserList);
        }
        else if(err){
          reject(err);
        }
       
      })
      
    })
  }
  getUserById(userEmail:string){
    // alert("in p")
    // console.log("companyName",this.companyName);
    console.log("userEmail",userEmail);

    this.userProfile = firebase.database().ref('XiraInfotech').child('UserProfile');

    const ref = this.userProfile.orderByChild('userEmail').equalTo(userEmail);
    return  new Promise((resolve, reject) => {
      ref.on('value', (resp) => {
        let UserList = snapshotToArray(resp);
        // let temparr = [];
        // for (var key in Catdata) {
        //   temparr.push(Catdata[key]);
        // }
        // console.log("tem", Catdata);
        resolve(UserList);
      })
      // .catch((err) => {
      //   reject(err);
      // })
    })
  }

  getUser(userId:string){
    this.userProfile = firebase.database().ref('XiraInfotech').child('UserProfile');

    const ref = this.userProfile.child(userId)
    return  new Promise((resolve, reject) => {
      ref.on('value', (resp,err) => {
        if(resp){
          let taskList = snapshotToArrayWithOutKey(resp);
          resolve(taskList);
        }else{
          reject(err)
        }
      
      })
    })
  }

  addTaskToDb(userId:string){
    // this.companyName=localStorage.getItem('companyName');

    this.userProfile = firebase.database().ref(this.companyName).child('UserProfile');

    var postref=this.userProfile.child(userId).child('Task');
    // var newPostRef=postref.push();
    return new Promise((resolve, reject) => {
      var newPostRef=postref.push();
      resolve(newPostRef);
    })
  }


  getAllTaskFromDb(userId:string){
    this.userProfile = firebase.database().ref(this.companyName).child('UserProfile');

    const ref = this.userProfile.child(userId).child('Task')
    return  new Promise((resolve, reject) => {
      ref.on('value', (resp) => {
        let taskList = snapshotToArray(resp);
        resolve(taskList);
      })
    })
  }

  getTaskbyTaskStatus(userId:string,taskStatus:string){
    // this.companyName=localStorage.getItem('companyName');

    this.userProfile = firebase.database().ref(this.companyName).child('UserProfile');
    console.log("taskStatus",taskStatus);
    const ref = this.userProfile.child(userId).child('Task').orderByChild('taskStatus').equalTo(taskStatus);
    return new Promise((resolve, reject)=>{
      // alert("123");
      ref.on('value',(res)=>{
        let action= snapshotToArray(res);
        console.log("timedata123", action);
        resolve(action);
      })
    })
  }

  getTimingsByDate(userId:string,date){
    // this.companyName=localStorage.getItem('companyName');

    this.userProfile = firebase.database().ref(this.companyName).child('UserProfile');
    console.log("taskStatus",date);
    const ref = this.userProfile.child(userId).child('Timings').orderByChild('key').equalTo(date);
    return new Promise((resolve, reject)=>{
      // alert("123");
      ref.on('value',(res)=>{
        let action= snapshotToArray(res);
        console.log("timedata123", action);
        resolve(action);
      })
    })
  }

  getTaskbyTaskId(userId:string,taskId:string){
    // this.companyName=localStorage.getItem('companyName');

    this.userProfile = firebase.database().ref(this.companyName).child('UserProfile');
    console.log("taskStatus",taskId);
    const ref = this.userProfile.child(userId).child('Task').child(taskId);
    return new Promise((resolve, reject)=>{
      // alert("123");
      ref.on('value',(res)=>{
        let action= snapshotToArray(res);
        console.log("timedata123", action);
        resolve(action);
      })
    })
  }
  getTaskbyProject(userId:string,projectname:string){
    // this.companyName=localStorage.getItem('companyName');

    this.userProfile = firebase.database().ref(this.companyName).child('UserProfile');
    console.log("projectname",projectname);
    const ref = this.userProfile.child(userId).child('Task').orderByChild('ProjectName').equalTo(projectname);
    return new Promise((resolve, reject)=>{
      // alert("123");
      ref.on('value',(res)=>{
        let action= snapshotToArray(res);
        console.log("timedata123", action);
        resolve(action);
      })
    })
  }

  getStatusOnAdminSide(taskStatus:string){
    // this.companyName=localStorage.getItem('companyName');

    this.userProfile = firebase.database().ref(this.companyName);
    const ref = this.userProfile.child('UserProfile').orderByChild('taskStatus').equalTo(taskStatus);
    return new Promise((resolve, reject)=>{
      // alert("123");
      ref.on('value',(res)=>{
        let action= snapshotToArray(res);
        console.log("timedata123", action);
        resolve(action);
      })
    })
  }

  addProjectsForUser(){
    // this.companyName=localStorage.getItem('companyName');

    this.projectList = firebase.database().ref(this.companyName).child('Projects');

    var postref=this.projectList
    // var newPostRef=postref.push();
    return new Promise((resolve, reject) => {
      var newPostRef=postref.push();
      resolve(newPostRef);
    })
  }

  getProjectforUser(){
    // this.companyName=localStorage.getItem('companyName');

    this.projectList = firebase.database().ref(this.companyName).child('Projects');

    const ref = this.projectList
    return  new Promise((resolve, reject) => {
      ref.on('value', (resp,err) => {
        if(resp){
          let UserList = snapshotToArray(resp);
          // let temparr = [];
          // for (var key in Catdata) {
          //   temparr.push(Catdata[key]);
          // }
          // console.log("tem", Catdata);
          resolve(UserList);
        }
        else if(err){
          reject(err);
        }
      
      })
      
    })
  }


  addDesignationForUser(){
    // this.companyName=localStorage.getItem('companyName');

    this.projectList = firebase.database().ref(this.companyName).child('Designation');

    var postref=this.projectList
    // var newPostRef=postref.push();
    return new Promise((resolve, reject) => {
      var newPostRef=postref.push();
      resolve(newPostRef);
    })
  }

  getDesignationforUser(){
    // this.companyName=localStorage.getItem('companyName');

    this.projectList = firebase.database().ref(this.companyName).child('Designation');

    const ref = this.projectList
    return  new Promise((resolve, reject) => {
      ref.on('value', (resp,err) => {
        if(resp){
          let UserList = snapshotToArray(resp);
          // let temparr = [];
          // for (var key in Catdata) {
          //   temparr.push(Catdata[key]);
          // }
          // console.log("tem", Catdata);
          resolve(UserList);
        }
        else if(err){
          reject(err);
        }
      
      })
      
    })
  }

  getDesignationByValue(designation:string){
    // this.companyName=localStorage.getItem('companyName');

    const ref = firebase.database().ref(this.companyName).child('Designation').orderByChild('designation').equalTo(designation);
    console.log("taskStatus",designation);
    // const ref = this.userProfile.child(userId).child('Task').orderByChild('taskStatus').equalTo(taskStatus);
    return new Promise((resolve, reject)=>{
      // alert("123");
      ref.on('value',(res)=>{
        let action= snapshotToArray(res);
        console.log("timedata123", action);
        resolve(action);
      })
    })
  }

  addCategoryForProject(){
    // this.companyName=localStorage.getItem('companyName');

    this.projectList = firebase.database().ref(this.companyName).child('ProjectCategory');

    var postref=this.projectList
    // var newPostRef=postref.push();
    return new Promise((resolve, reject) => {
      var newPostRef=postref.push();
      resolve(newPostRef);
    })
  }
  getcategory(){
    const ref = firebase.database().ref(this.companyName).child('ProjectCategory');
    // console.log("taskStatus",category);
    // const ref = this.userProfile.child(userId).child('Task').orderByChild('taskStatus').equalTo(taskStatus);
    return new Promise((resolve, reject)=>{
      // alert("123");
      ref.on('value',(res)=>{
        let action= snapshotToArray(res);
        console.log("timedata123", action);
        resolve(action);
      })
    })
  }
  getcategoryByProjectName(ProjectName:string){
    const ref = firebase.database().ref(this.companyName).child('Projects').orderByChild('projectName').equalTo(ProjectName);
    // console.log("taskStatus",category);
    // const ref = this.userProfile.child(userId).child('Task').orderByChild('taskStatus').equalTo(taskStatus);
    return new Promise((resolve, reject)=>{
      // alert("123");
      ref.on('value',(res)=>{
        let action= snapshotToArray(res);
        console.log("timedata123", action);
        resolve(action);
      })
    })
  }

  getcategoryByValue(category:string){
    // this.companyName=localStorage.getItem('companyName');

    const ref = firebase.database().ref(this.companyName).child('ProjectCategory').orderByChild('Category').equalTo(category);
    console.log("taskStatus",category);
    // const ref = this.userProfile.child(userId).child('Task').orderByChild('taskStatus').equalTo(taskStatus);
    return new Promise((resolve, reject)=>{
      // alert("123");
      ref.on('value',(res)=>{
        let action= snapshotToArray(res);
        console.log("timedata123", action);
        resolve(action);
      })
    })
  }

  // getSubCategoryByCategory(category:string){
  //   const ref = firebase.database().ref(this.companyName).child('ProjectCategory').orderByChild('Category').equalTo(category);

  // }


  // getcategorybyName()


  assignProjectToUser(userId:string){
    // this.companyName=localStorage.getItem('companyName');

    this.userProfile = firebase.database().ref(this.companyName).child('UserProfile');

    const ref = this.userProfile.child(userId).child('Project')
    return new Promise((resolve, reject) => {
      var newPostRef=ref.push();
      resolve(newPostRef);
    })
  }

  getAssignedProjects(userId:string){
    // this.companyName=localStorage.getItem('companyName');
    console.log("com",this.companyName);
    this.userProfile = firebase.database().ref(this.companyName).child('UserProfile');

    const ref = this.userProfile.child(userId).child('Project')
    return  new Promise((resolve, reject) => {
      ref.on('value', (resp) => {
        let UserList = snapshotToArray(resp);
        // let temparr = [];
        // for (var key in Catdata) {
        //   temparr.push(Catdata[key]);
        // }
        // console.log("tem", Catdata);
        resolve(UserList);
      })
      // .catch((err) => {
      //   reject(err);
      // })
    })
  }

  getCompanyNameByName(companyName:string){
    // this.companyName=localStorage.getItem('companyName');

    const ref = firebase.database().ref('Company').orderByChild('registeredCompany').equalTo(companyName);
    return new Promise((resolve, reject)=>{
      // alert("123");
      ref.on('value',(res)=>{
        let action= snapshotToArray(res);
        // console.log("timedata123", action);
        resolve(action);
      })
    })
  }

  // addJobStartTime(userId:string){
  //   // this.companyName=localStorage.getItem('companyName');

  //   this.userProfile = firebase.database().ref(this.companyName).child('UserProfile');
  //   var postref=this.userProfile.child(userId).child('Timings');
  //   // var newPostRef=postref.push();
  //   return new Promise((resolve, reject) => {
  //     var newPostRef=postref.push();
  //     resolve(newPostRef);
  //   })
  // }
  // addJobEndTime(userId:string,date){
  //   // this.companyName=localStorage.getItem('companyName');

  //   this.userProfile = firebase.database().ref(this.companyName).child('UserProfile');
  //   var postref=this.userProfile.child(userId).child("timings");
  //   // var newPostRef=postref.push();
  //   return new Promise((resolve, reject) => {
  //     var newPostRef=postref.push();
  //     resolve(newPostRef);
  //   })
  // }
  // addJobLunchStartTime(userId:string){
  //   // this.companyName=localStorage.getItem('companyName');
  //   this.userProfile = firebase.database().ref(this.companyName).child('UserProfile');
  //   var postref=this.userProfile.child(userId).child('timings');
  //   // var newPostRef=postref.push();
  //   return new Promise((resolve, reject) => {
  //     var newPostRef=postref.push();
  //     resolve(newPostRef);
  //   })
  // }
  // addJobLunchEndTime(userId:string){
  //   // this.companyName=localStorage.getItem('companyName');

  //   this.userProfile = firebase.database().ref(this.companyName).child('UserProfile');
  //   var postref=this.userProfile.child(userId).child('timings');
  //   // var newPostRef=postref.push();
  //   return new Promise((resolve, reject) => {
  //     var newPostRef=postref.push();
  //     resolve(newPostRef);
  //   })
  // }

  getJobStartTime(userId:string){
    // this.companyName=localStorage.getItem('companyName');
    console.log("com",this.companyName);
    this.userProfile = firebase.database().ref(this.companyName).child('UserProfile');

    const ref = this.userProfile.child(userId).child('Timings')
    return  new Promise((resolve, reject) => {
      ref.on('value', (resp) => {
        let UserList = snapshotToArray(resp);
        // let temparr = [];
        // for (var key in Catdata) {
        //   temparr.push(Catdata[key]);
        // }
        // console.log("tem", Catdata);
        resolve(UserList);
      })
      // .catch((err) => {
      //   reject(err);
      // })
    })
  }
  

  isAdmin() {
    this.userType=localStorage.getItem('type');
    console.log("this.userType",this.userProfile=='admin');
    if(this.userProfile=='admin'){
      return true;

    }else if(this.userProfile=='user'){
      return false;
    }
  }
 

  }

export const snapshotToArray = snapshot => {
  let returnArr = [];
  // let success;
  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    // console.log("Author: " + item.key);
    // console.log("Title: " + item.password);
    // if(item.userName==this.data.userName && item.password==this.data.password){

    //   alert("succes");
    //   console.log("succes login");
    //   return success;
    // }
    returnArr.push(item);
  });
  // return success;
  return returnArr;

}
export const snapshotToArrayWithOutKey = snapshot => {
  let returnArr = [];
  // let success;
  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    // item.key = childSnapshot.key;
    // console.log("Author: " + item.key);
    // console.log("Title: " + item.password);
    // if(item.userName==this.data.userName && item.password==this.data.password){

    //   alert("succes");
    //   console.log("succes login");
    //   return success;
    // }
    returnArr.push(item);
  });
  // return success;
  return returnArr;

}

