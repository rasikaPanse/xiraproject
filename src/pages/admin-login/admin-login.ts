import { Component ,ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {  FormGroup, Validators ,FormBuilder} from '@angular/forms';
import firebase from 'firebase';
import { Nav, Platform } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { AdminMainPage } from '../admin-main/admin-main';
import { AdminRegistrationPage } from '../admin-registration/admin-registration';
import { UserServicesProvider } from '../../providers/user-service/user-services';
// import { UserServicesProvider } from '../../providers/user-services/user-services';
/**
 * Generated class for the AdminLoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-login',
  templateUrl: 'admin-login.html',
})
export class AdminLoginPage {
  @ViewChild(Nav) nav: Nav;

  loginForm: FormGroup;
  isenabled:boolean=false;
  public userdet:any;
  public errorMess:any;


  data = {
    adminEmail: "",
    password: "",

  };
  constructor(public alertCtrl:AlertController,public userServicesProvider:UserServicesProvider, public navCtrl: NavController, public navParams: NavParams,private formBuilder: FormBuilder) {
    this.loginForm = this.formBuilder.group({
      email: ['',Validators.compose([
        Validators.pattern(regexValidators.email),
        Validators.required
      ])],
      password: ['',Validators.compose([
        Validators.pattern(regexValidators.password),
        Validators.required
      ])
       
      ],
    });
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad AdminLoginPage');
  }

  goToAdminMainPage(){
    // alert("1");
    let email=this.data.adminEmail,
    password=this.data.password
    // console.log(email);
    // console.log(password);
    this.userServicesProvider.getAdmin(email).then((resss:any)=>{
      console.log("resss",resss);
      if(resss.length>0){
        this.userServicesProvider.loginWithEmailAndPassword(email,password).then((response:any)=>{
          if(response!==null){
            localStorage.setItem("userId", response.user.uid);
            this.isenabled = false;
    
            if(response.operationType== 'signIn'){
              localStorage.setItem("user", response.operationType);
              localStorage.setItem("type","admin");
              localStorage.setItem("userEmail", response.user.email);
            }
            this.userdet = firebase.auth().currentUser;
            if (this.userdet != null) {
              // this.navCtrl.setRoot(AdminMainPage);
              this.navCtrl.setRoot('MenuPage');
    
              // this.navCtrl.push(AdminMainPage);
            }
    
          }
         
        },error=>{
          // console.log("error fgdrgregistr",error.message);
          this.isenabled=true;
          this.errorMess=error.message;
        })
      }
      else{
        alert("Not a admin mail id");
      }
    })
   
  }

  goToRegistrationPage(){
    this.navCtrl.push(AdminRegistrationPage)
  }
  showAlert() {
    const alert = this.alertCtrl.create({
      title: 'Attention!',
      subTitle: 'Check your mail for resetting password Link has been send successfully',
      buttons: ['OK']
    });
    alert.present();
  }

  resetPassword(email: string) {
    var auth = firebase.auth();
    return auth.sendPasswordResetEmail(email)
      .then(() => {console.log("email sent");
      this.showAlert()
    })
      .catch((error) => console.log(error))
}

showPrompt() {
  const prompt = this.alertCtrl.create({
    title: 'Login',
    message: "Enter a email id for which you want to reset password",
    inputs: [
      {
        name: 'EmailID',
        placeholder: 'EmailID'
      },
    ],
    buttons: [
      {
        text: 'Cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Save',
        handler: data => {
          console.log('Saved clicked',data.EmailID);
          this.resetPassword(data.EmailID);
        }
      }
    ]
  });
  prompt.present();
}

}

const PURE_EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

// Passwords should be at least 8 characters long and should contain one number, one character and one special character.
const PASSWORD_REGEXP = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;

export const regexValidators = {
  email: PURE_EMAIL_REGEXP,
  password: PASSWORD_REGEXP
};