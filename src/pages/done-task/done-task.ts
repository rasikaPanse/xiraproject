import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserServicesProvider } from '../../providers/user-service/user-services';
import * as moment from 'moment';
import firebase from 'firebase';

/**
 * Generated class for the DoneTaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-done-task',
  templateUrl: 'done-task.html',
})
export class DoneTaskPage {
  public userId:any;
  public doneTask:any;
  public userProfile;
  constructor(public navCtrl: NavController, public navParams: NavParams,public userServicesProvider:UserServicesProvider) {
    this.userId=localStorage.getItem('userId');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DoneTaskPage');
  }

  ionViewWillEnter(){
    this.userServicesProvider.getTaskbyTaskStatus(this.userId,'done').then((response:any)=>{
      console.log("rsponse",response);
      this.doneTask=response;
    })

    this.userServicesProvider.getUserOnAdminSide().then((result:any)=>{
      console.log("result",result);
      this.userProfile=result;
    })
  }

  detailsTask(task, index) {
    console.log("index home", index)
    this.navCtrl.push('DetailedTaskPage', { taskdetails: task, index: index,flag:'done' })
  }


}
