import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { UserServicesProvider } from '../../providers/user-service/user-services';

/**
 * Generated class for the ProjectWiseReportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-project-wise-report',
  templateUrl: 'project-wise-report.html',
})
export class ProjectWiseReportPage {
  public projectTask:any=[];
  public projectList:any;
  constructor(public alertCtrl:AlertController,public navCtrl: NavController, public navParams: NavParams,public userServicesProvider:UserServicesProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProjectWiseReportPage');
  }

  getProjectReport(item){
    // alert("12");
    this.projectTask=[];
    console.log("project",item);
    this.navCtrl.push('UserPage',{item:item})
    // item.checkclicked=true
    // this.checkclicked=true;
    // if(this.checkclicked){
    //   this.checkclicked=false
    // }else if(!this.checkclicked){
    //   this.checkclicked=true
    // }
    // this.userServicesProvider.getUserOnAdminSide().then((response:any)=>{
    //   console.log("response",response);
    //   console.log("length of repsonse",response.length);
    //   for(let i=0;i<response.length;i++){
    //     // alert("1");
    //     this.userServicesProvider.getTaskbyProject(response[i].key,item.projectName).then((result:any)=>{
    //       console.log("result1234",result);
    //       // alert("6");
    //       result.map((resp:any)=>{
    //         // alert("3");
    //         console.log("respnse.......",resp);
    //         // response.userId=res[i].key;
    //         resp.username=response[i].firstName;
            
    //         this.projectTask.push(resp);
    //        //  this.completedTask.push({userId:res[i].key})
    //         console.log("projectTask",this.projectTask);
    //       })
          
    //     })
    //   }
    // })
    // console.log("checkclicked",this.projectTask)
  }
  doRefresh(refresher){
    console.log('begin sycn operaytion',refresher);
    this.ionViewDidLoad();
    setTimeout(()=>{
      console.log('Async operation has ended');
      refresher.complete();
    },2000);
  }


  addMultipleUser(item){
    console.log("item",item);
    this.userServicesProvider.getUserOnAdminSide().then((resss:any)=>{
      console.log("resssss",resss);
      let alert = this.alertCtrl.create();
      alert.setTitle('AssignProject');
      for (let i = 0; i < resss.length; i++) {
        console.log("");
        alert.addInput({
          type: 'checkbox',
          label: resss[i].firstName,
          value: resss[i].key,
          checked: false
        });
      }


      alert.addButton('Cancel');
      alert.addButton({
        text: 'Okay',
        handler: data => {
          console.log('Checkbox data:', data);
          console.log("datalength",data.length);
          for(let j=0;j<data.length;j++){
            this.userServicesProvider.assignProjectToUser(data[j]).then((res:any)=>{
              res.set({
                projectName:item.projectName,
                projectDescription:item.projectDescription,
                key:res.key,
                category:item.category,
                projectKey:item.key
              })
            })
          }

          // this.userServicesProvider.assignProjectToUser(user.key).then((res:any)=>{
          //   res.set({
          //     projectName:data,
          //     key:res.key
          //   })
            
        // })
          // this.userServicesProvider.
          // this.testCheckboxOpen = false;
          // this.testCheckboxResult = data;
          // this.userServicesProvider.addProjectsForUser().then((response: any) => {
          //   response.set({
          //     projectName: this.project,
          //     category: category,
          //     projectDescription: data,
          //     key: response.key
          //   })
          //   this.navCtrl.setRoot('AdminMainPage');
          // })


          // this.projectListforadding.set({
          //   projectName:this.project,
          // })

        }
      });
      alert.present();
    })
    
      // this.userServicesProvider.getcategoryByValue(category).then((response: any) => {
      //   console.log("response", response[0].workToDo.length);
      //   let alert = this.alertCtrl.create();
      //   alert.setTitle('services');
      //   for (let i = 0; i < response[0].workToDo.length; i++) {
      //     console.log("");
      //     alert.addInput({
      //       type: 'checkbox',
      //       label: response[0].workToDo[i],
      //       value: response[0].workToDo[i],
      //       checked: false
      //     });
      //   }
  
  
      //   alert.addButton('Cancel');
      //   alert.addButton({
      //     text: 'Okay',
      //     handler: data => {
      //       console.log('Checkbox data:', data);
      //       // this.testCheckboxOpen = false;
      //       // this.testCheckboxResult = data;
      //       this.userServicesProvider.addProjectsForUser().then((response: any) => {
      //         response.set({
      //           projectName: this.project,
      //           category: category,
      //           projectDescription: data,
      //           key: response.key
      //         })
      //         this.navCtrl.setRoot('AdminMainPage');
      //       })
  
  
      //       // this.projectListforadding.set({
      //       //   projectName:this.project,
      //       // })
  
      //     }
      //   });
      //   alert.present();
      // })
    
  }

  ionViewWillEnter(){
    this.userServicesProvider.getProjectforUser().then((result:any)=>{
      console.log("resut",result);
      this.projectList=result;
    })
  }

}
