import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProjectWiseReportPage } from './project-wise-report';

@NgModule({
  declarations: [
    ProjectWiseReportPage,
  ],
  imports: [
    IonicPageModule.forChild(ProjectWiseReportPage),
  ],
  exports:[
    ProjectWiseReportPage
  ],
})
export class ProjectWiseReportPageModule {}
