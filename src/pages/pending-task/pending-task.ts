import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { UserServicesProvider } from '../../providers/user-service/user-services';
import * as moment from 'moment';
import firebase from 'firebase';
import * as _ from 'lodash';

/**
 * Generated class for the PendingTaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pending-task',
  templateUrl: 'pending-task.html',
})
export class PendingTaskPage {
  public userId:any;
  pendingTask:any;
  pendingTask1:any;
  pendingTask2:any;
  CurrentTime:any;
  companyName:any;
  userProfile:any;
  newtimetaken:any;
  public todayDate = moment().format("MM-DD-YYYY");       
  constructor(public alertCtrl:AlertController,public navCtrl: NavController, public navParams: NavParams,public userServicesProvider:UserServicesProvider) {
    this.userId=localStorage.getItem('userId');
  this.companyName='XiraInfotech';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PendingTaskPage');
  }
  ionViewWillEnter(){
    this.userServicesProvider.getTaskbyTaskStatus(this.userId,'pause').then((response:any)=>{
      console.log("rsponse",response);
      this.pendingTask1=response;
    })
    this.userServicesProvider.getTaskbyTaskStatus(this.userId,'pending').then((response:any)=>{
      console.log("rsponse",response);
      this.pendingTask2=response;
    })

    setTimeout(()=>{
      console.log("this.pendingTask1",this.pendingTask1);
      console.log('this.pendingTask2',this.pendingTask2);
      this.pendingTask= _.concat(this.pendingTask1,this.pendingTask2);
      console.log("tjis.pendingtask",this.pendingTask);
    },2000);

    


    this.userServicesProvider.getUserOnAdminSide().then((result:any)=>{
      console.log("result",result);
      this.userProfile=result;
    })
  }



  detailsTask(task, index) {
    console.log("index home", index)
    this.navCtrl.push('DetailedTaskPage', { taskdetails: task, index: index,flag:'pending' })
  }


  startTask(task) {
    this.CurrentTime = moment().format("HH:mm");
    console.log("CurrentTime", this.CurrentTime);
    console.log("task", task);
    // console.log("taskName",this.taskName);
    // console.log("this.userId",this.userId);
    console.log("todayDate", this.todayDate);
    console.log("CurrentTime", this.CurrentTime);
    this.userServicesProvider.getTaskbyTaskStatus(this.userId, "started").then((response: any) => {
      console.log("response1", response[0]);

      let diff = moment(this.CurrentTime, 'HH:mm').diff(moment(response[0].startTime, 'HH:mm'));
      console.log("diff", diff);
      let d = moment.duration(diff);
      console.log("d", d);
      let m = Math.floor(d.asHours()) + moment.utc(diff).format(":mm");
      console.log("m", m);

      this.userServicesProvider.getTaskbyTaskStatus(this.userId, "pause").then((res: any) => {
        console.log("response1", res);
      })

      // response[0].update({ taskStatus: "pause" });
      firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).child('Task').child(response[0].key)
        .update({
          taskStatus: "pause",
          timeTaken: m,
          endTime: this.CurrentTime
        });
    }).catch((error: any) => console.log(error));

    firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).child('Task').child(task.key)
      .update({
        taskStatus: "started",
        startDate: this.todayDate,
        startTime: this.CurrentTime
      });
    this.ionViewWillEnter();
    this.ionViewDidLoad();
  }

  taskcomplete(taskkey) {
    // alert(taskkey);
    this.showPrompt(taskkey);



    // this.sendNotification();
  }

  taskPause(taskkey) {
    // alert(taskkey);
    // console.log("taskkey",taskkey);
    // console.log("133");
    // alert("2");
    // console.log("timeTaken",taskkey.timeTaken);

    this.userServicesProvider.getTaskbyTaskStatus(this.userId, "started").then((response: any) => {
      // console.log("response1", response[0]);

      let diff = moment(this.CurrentTime, 'HH:mm').diff(moment(response[0].startTime, 'HH:mm'));
      // console.log("diff", diff);
      let d = moment.duration(diff);
      // console.log("d", d);
      let m = Math.floor(d.asHours()) + moment.utc(diff).format(":mm");
      // console.log("m", m);
      response[0].update({ taskStatus: "pause" });
      
      firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).child('Task').child(taskkey.key)
        .update({
          taskStatus: "pause",
          timeTaken: m,
          endTime: this.CurrentTime
        });
      // firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).child('Task').child(response[0].key)
      //   .update({
      //     taskStatus: "pause",
      //     timeTaken: m,
      //     endTime: this.CurrentTime
      //   });
    }).catch((error: any) => console.log(error))
    this.ionViewWillEnter();
    this.ionViewDidLoad();

  }

  transferTask(taskey){
    // console.log("taskqqq",taskey);
    this.TransferTaskToUser(taskey);
  }

  TransferTaskToUser(taskey) {
      let alert = this.alertCtrl.create();
      alert.setTitle('Lightsaber color');
    for (let i=0;i<this.userProfile.length;i++){
      alert.addInput({
        type: 'radio',
        label: this.userProfile[i].firstName,
        value: this.userProfile[i].key,
        checked: false
      });
    }
  
      alert.addButton('Cancel');
      alert.addButton({
        text: 'OK',
        handler: data => {
          // console.log("data",data);
          // console.log("taskey",taskey);
          this.reasonTransfer(data,taskey);    
      
          // this.userServicesProvider.addTaskToDb(data).then((result:any)=>{
          //   result.set({
          //     Priority:taskey.Priority,
          //     ProjectName:taskey.ProjectName,
          //     RemarkByMe:taskey.RemarkByMe,
          //     TaskDescription:taskey.TaskDescription,
          //     TaskName:taskey.TaskName,
          //     assignedBy:taskey.assignedBy,
          //     assignedDate:taskey.assignedDate,
          //     assignedTime:taskey.assignedTime,
          //     taskStatus:taskey.taskStatus,
          //     taskTransferedBy:data,
              
          //   })
          // })
          // this.testRadioOpen = false;
          // this.testRadioResult = data;
        }
      });
      alert.present();
  }

  reasonTransfer(dataname,taskey){
    // console.log("data",dataname);
    // console.log("task",taskey)
      const prompt = this.alertCtrl.create({
        title: 'Specify Reason',
        message: "Please specify reason for transfer this task",
        inputs: [
          {
            name: 'Reason',
            placeholder: 'Reason'
          },
        ],
        buttons: [
          {
            text: 'Cancel',
            handler: data => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Save',
            handler: data => {
              console.log('Saved clicked');
              this.userServicesProvider.addTaskToDb(dataname).then((result:any)=>{
                result.set({
                  Priority:taskey.Priority,
                  ProjectName:taskey.ProjectName,
                  TaskDescription:taskey.TaskDescription,
                  TaskName:taskey.TaskName,
                  assignedBy:taskey.assignedBy,
                  assignedDate:taskey.assignedDate,
                  assignedTime:taskey.assignedTime,
                  taskStatus:taskey.taskStatus,
                  taskTransferedBy:dataname,
                  taskTransferedReason:data.reason
                  
                })
              })
            }
          }
        ]
      });
      prompt.present();
    
  }

  showPrompt(taskkey) {
    const prompt = this.alertCtrl.create({
      title: 'Remark',
      message: "Please Enter Remark",
      inputs: [
        {
          name: 'Remark',
          placeholder: 'Remark'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log('Saved clicked', data);
            this.CurrentTime = moment().format("HH:mm");
            // console.log("CurrentTime", taskkey);

            let diff = moment(this.CurrentTime, 'HH:mm').diff(moment(taskkey.startTime, 'HH:mm'));
            // console.log("diff", diff);
            let d = moment.duration(diff);
            // console.log("d", d);
            let m = Math.floor(d.asHours()) + moment.utc(diff).format(":mm");
            // console.log("m", m);
            firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).child('Task').child(taskkey.key)
              .update({
                taskStatus: "completed",
                endTime: this.CurrentTime,
                timeTaken: m,
                RemarkByMe: data.Remark
              });
            this.ionViewWillEnter();
            this.ionViewDidLoad();
          }
        }
      ]
    });
    prompt.present();
  }

  doRefresh(refresher){
    console.log('begin sycn operaytion',refresher);
    this.ionViewDidLoad();
    setTimeout(()=>{
      console.log('Async operation has ended');
      refresher.complete();
    },2000);
  }


}
