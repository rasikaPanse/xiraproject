import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PendingTaskPage } from './pending-task';

@NgModule({
  declarations: [
    PendingTaskPage,
  ],
  imports: [
    IonicPageModule.forChild(PendingTaskPage),
  ],
  exports:[
    PendingTaskPage
  ]
})
export class PendingTaskPageModule {}
