import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController,ActionSheetController } from 'ionic-angular';

import { UserServicesProvider } from '../../providers/user-service/user-services';
import firebase from 'firebase';
import { UserRegistrationPage } from '../user-registration/user-registration';
import { AssignTaskPage } from '../assign-task/assign-task';
import { UserprofilePage } from '../userprofile/userprofile';
import { HomePage } from '../home/home';
/**
 * Generated class for the AdminMainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-main',
  templateUrl: 'admin-main.html',
})
export class AdminMainPage {

  userDetails:any;
  projectListforadding:any;
  companyName:any;
  selectDropDown:boolean=false;
  projectList:any;
  testRadioOpen;
  testRadioResult;
  constructor(public navCtrl: NavController, public navParams: NavParams,public alertCtrl: AlertController,public userServicesProvider:UserServicesProvider,public actionSheetCtrl: ActionSheetController) {
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad AdminMainPage');
    // console.log('ionViewDidLoad AdminMainPage');
    this.userServicesProvider.getUserOnAdminSideByStatus('online').then((userResponse:any)=>{
      // console.log("userResponse",userResponse);
      // this.companyName=localStorage.getItem('companyName');
      this.companyName='XiraInfotech';
      this.userDetails=userResponse;
      this.projectListforadding = firebase.database().ref(this.companyName).child('Projects');
      // console.log("userDetails",this.userDetails);
    })
  }

  ionViewWillEnter(){
    this.userServicesProvider.getProjectforUser().then((projectlistfromdb:any)=>{
      // console.log("helo",projectlistfromdb);
      this.projectList=projectlistfromdb;

    })
  }
  testpage(){
    this.navCtrl.push(HomePage);
  }
  doRefresh(refresher){
    console.log('begin sycn operaytion',refresher);
    this.ionViewDidLoad();
    setTimeout(()=>{
      console.log('Async operation has ended');
      refresher.complete();
    },2000);
  }

  assignProject(user) {
    let alert = this.alertCtrl.create();
    alert.setTitle('Select Project');
    for(let i=0;i<this.projectList.length;i++){
      
    
    alert.addInput({
      type: 'radio',
      label: this.projectList[i].projectName,
      value: this.projectList[i].projectName,
      checked: false
    });
  }
   
    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        this.testRadioOpen = false;
        this.testRadioResult = data;
        // console.log("data",user);
        this.userServicesProvider.assignProjectToUser(user.key).then((res:any)=>{
          res.set({
            projectName:data,
            key:res.key
          })
          
      })
    }
    });
    alert.present();
  }

  assignTask(user1){
    // console.log('uzer',user1)
    this.navCtrl.push(AssignTaskPage,{userDetails:user1});
  }

  addUser(){
    // alert("7");
    this.navCtrl.push(UserRegistrationPage);
  }
  getUserTaskDetails(task){
    // console.log("task",task);
    this.navCtrl.push(UserprofilePage,{userdetails:task});
  }
  openPage(viewPage){
    // alert("l");
    // console.log("viewPage",viewPage);
    if(viewPage=="TaskInreview"){
      // this.navCtrl.push(ReviewTaskPage);
    }
  }

  // addProject(){
  //   console.log("project",this.project);
    
  //   this.userServicesProvider.addProjectsForUser().then((response:any)=>{
  //     response.set({
  //       projectName:this.project,
  //       key:response.key
  //     })
  //   })
  //     // this.projectListforadding.set({
  //     //   projectName:this.project,
  //     // })
   
  // }

}
