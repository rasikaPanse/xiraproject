import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminMainPage } from './admin-main';

@NgModule({
  declarations: [
    AdminMainPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminMainPage),
  ],
  exports:[
    AdminMainPage
  ]
})
export class AdminMainPageModule {}
