import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,AlertController} from 'ionic-angular';
import {  FormGroup, Validators } from '@angular/forms';
import { FormBuilder} from '@angular/forms';
import { UserServicesProvider } from '../../providers/user-service/user-services';
import firebase from 'firebase';
// import { TaskDifferentChoicePage } from '../task-different-choice/task-different-choice';
// import { MenuPage } from '../menu/menu';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  userloginForm: FormGroup;
  isenabled:boolean=false;
  public userdet:any;
  public errorMess:any;


  data = {
    userEmail: "",
    password: "",

  }
  constructor(public alertCtrl:AlertController,public navCtrl: NavController, public navParams: NavParams,public userServicesProvider:UserServicesProvider,private formBuilder: FormBuilder) {
    this.userloginForm = this.formBuilder.group({
      email: ['',Validators.compose([
        Validators.pattern(regexValidators.email),
        Validators.required
      ])],
      password: ['',Validators.compose([
        Validators.pattern(regexValidators.password),
        Validators.required
      ])
       
      ]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login(){
    let email=this.data.userEmail,
    password=this.data.password

    this.userServicesProvider.getUserById(email).then((resss:any)=>{
      // alert("ddd");
      console.log("ress login user",resss[0]);
      this.userServicesProvider.loginWithEmailAndPassword(email,password).then((response:any)=>{
        // console.log("response",response);
        if(response!==null){
          localStorage.setItem("userId", response.user.uid);
          this.isenabled = false;
  
          if(response.operationType== 'signIn'){
            localStorage.setItem("user", response.operationType);
            localStorage.setItem("type","user");
            localStorage.setItem("userEmail", response.user.email);
            // localStorage.setItem("companyName", resss[0].company);

  
            
          }
          this.userdet = firebase.auth().currentUser;
          if (this.userdet != null) {
            // this.navCtrl.push(TabsPage);
            // this.userServicesProvider.getUserById(response.user.uid).then((res:any)=>{
              // console.log("ionic12",this.userdet);
            // })
            // alert("123");
              // this.navCtrl.setRoot(MenuPage);
              // localStorage.
              this.navCtrl.setRoot('MenuPage');
            // this.navCtrl.push(TaskDifferentChoicePage);
          }
  
        }
       
      },error=>{
        console.log("error fgdrgregistr",error.message);
        this.isenabled=true;
        this.errorMess=error.message;
      })
    })
    // company=this.data.company
    // console.log(email);
    // console.log(password);
    // console.log(company);
    // localStorage.setItem("co?mpanyName", company);


  
  }
  showAlert() {
    const alert = this.alertCtrl.create({
      title: 'Attention!',
      subTitle: 'Check your mail for resetting password Link has been send successfully',
      buttons: ['OK']
    });
    alert.present();
  }

  resetPassword(email: string) {
    var auth = firebase.auth();
    return auth.sendPasswordResetEmail(email)
      .then(() => {console.log("email sent");
      this.showAlert()
    })
      .catch((error) => console.log(error))
}

showPrompt() {
  const prompt = this.alertCtrl.create({
    title: 'Login',
    message: "Enter a email id for which you want to reset password",
    inputs: [
      {
        name: 'EmailID',
        placeholder: 'EmailID'
      },
    ],
    buttons: [
      {
        text: 'Cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Save',
        handler: data => {
          console.log('Saved clicked',data.EmailID);
          this.resetPassword(data.EmailID);
        }
      }
    ]
  });
  prompt.present();
}

}

const PURE_EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

// Passwords should be at least 8 characters long and should contain one number, one character and one special character.
const PASSWORD_REGEXP = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;

export const regexValidators = {
  email: PURE_EMAIL_REGEXP,
  password: PASSWORD_REGEXP
};


