import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserServicesProvider } from '../../providers/user-service/user-services';
import * as moment from 'moment';
/**
 * Generated class for the AddTaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-task',
  templateUrl: 'add-task.html',
})
export class AddTaskPage {
  public userDetails:any=[];
  public userId:any;
  public userProject:any;
  public CurrentTime:any;
  public projectName:any;
  public taskName:any;
  public todayDate = moment().format("MM-DD-YYYY");
  public taskDescription:any;
  public Priority:any;


  constructor(public navCtrl: NavController, public navParams: NavParams, public userServicesProvider:UserServicesProvider) {
    this.userId=localStorage.getItem('userId');
    // console.log("userId",this.userId);
  

  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad AddTaskPage');
  
      this.userServicesProvider.getAssignedProjects(this.userId).then((Response:any)=>{
        // console.log("Response",Response);
        this.userProject= Response;
      })
    
    
  }


  AddTask(){
    // alert("add");
    this.CurrentTime= moment().format("HH:mm");
    // console.log("projectName",this.projectName);
    // console.log("taskName",this.taskName);
    // console.log("this.userId",this.userId);
    // console.log("todayDate",this.todayDate);
    // console.log("CurrentTime",this.CurrentTime);
  
    // console.log("response1",this.userId);

    this.userServicesProvider.addTaskToDb(this.userId).then((ress:any)=>{
      ress.set({
        assignedBy:"self",
        ProjectName: this.projectName,
        assignedDate:this.todayDate,
        TaskName:this.taskName,
        TaskDescription:this.taskDescription,
        assignedTime:this.CurrentTime,
        Priority:this.Priority,
        taskStatus:"given",
        userId:this.userId
      });
    })


   this.navCtrl.setRoot('AdminAssignTaskPage')
  //  this.navCtrl.push(TabsPage,{index: "2"});
  }

}
