import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import $ from "jquery";
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {
    
  }
  ionViewDidLoad(){
    $('body').scrollspy({
      target: '.bs-docs-sidebar',
      offset: 40
  });
  $("#sidebar").affix({
      offset: {
        top: 60
      }
  });
  }
  
}
