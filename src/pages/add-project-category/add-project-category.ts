import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { UserServicesProvider } from '../../providers/user-service/user-services';

import { FormBuilder, FormArray, FormGroup, Validators } from '@angular/forms';

/**
 * Generated class for the AddProjectCategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-project-category',
  templateUrl: 'add-project-category.html',
})
export class AddProjectCategoryPage {
  public form: FormGroup;
  // public Category: any;
  // public workToDo1: any;
  public workToDo2: any;
  public workToDo3: any;
  public workToDo4: any;
  public workToDo: any;

  public item1: boolean = false;
  public item2: boolean = false;
  public item3: boolean = false;

  constructor(public navCtrl: NavController, private _FB: FormBuilder, public toastCtrl: ToastController, public navParams: NavParams, public userServicesProvider: UserServicesProvider) {
    this.form = this._FB.group({
      Category: ['', Validators.required],
      workToDo1: this._FB.array([
        this.initTechnologyFields()
      ])
    });
  }

  initTechnologyFields(): FormGroup {
    return this._FB.group({
      Category: ['', Validators.required]
    });
  }
  addNewInputField(): void {
    const control = <FormArray>this.form.controls.workToDo1;
    control.push(this.initTechnologyFields());
  }
  removeInputField(i: number): void {
    const control = <FormArray>this.form.controls.workToDo1;
    control.removeAt(i);
  }
  manage(val: any): void {
    console.log("njkkf",val);
    this.userServicesProvider.getcategoryByValue(val.Category).then((res: any) => {
      console.log("res",res);
      // console.log("reslength",res.length);

      if (res.length == 0) {
        this.userServicesProvider.addCategoryForProject().then((response: any) => {
          // alert("fdd");
          // console.log("res",response);
          response.set({
            Category: val.Category,
            workToDo: val.workToDo1
          })

        })
        this.presentToastfornew(val.Category);
        this.navCtrl.setRoot('AdminMainPage');

      }
      else {
        this.presentToast(val.Category);
      }
    })
  }

  add_fields1() {
    this.item1 = true;
  }
  add_fields2() {
    this.item2 = true;
  }
  add_fields3() {
    this.item3 = true;
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad AddProjectCategoryPage');
  }

  // addCategory() {

  //   // console.log("Category",this.Category);
  //   // console.log("workToDo",this.workToDo1);
  //   // console.log("workToDo",this.workToDo2);

  //   // console.log("workToDo",this.workToDo3);

  //   // console.log("workToDo",this.workToDo);
  //   this.workToDo1 = this.workToDo.split(',');
  //   // console.log("wwwwww",this.workToDo1);
  //   // if(this.workToDo1=='undefined'){
  //   //   this.workToDo.push(this.workToDo2);
  //   //   this.workToDo.push(this.workToDo3);
  //   //   this.workToDo.push(this.workToDo4);
  //   // }else
  //   // if(this.workToDo2='undefined'){
  //   //   this.workToDo.push(this.workToDo1);
  //   //   this.workToDo.push(this.workToDo3);
  //   //   this.workToDo.push(this.workToDo4);
  //   // }else
  //   // if(this.workToDo3='undefined'){
  //   //   this.workToDo.push(this.workToDo1);
  //   //   this.workToDo.push(this.workToDo2);
  //   //   this.workToDo.push(this.workToDo4);
  //   // }else
  //   // if(this.workToDo4='undefined'){
  //   //   this.workToDo.push(this.workToDo1);
  //   //   this.workToDo.push(this.workToDo2);
  //   //   this.workToDo.push(this.workToDo3);
  //   // }else if(this.workToDo1=='undefined' && this.workToDo2=='undefined'){
  //   //   this.workToDo.push(this.workToDo3);
  //   //   this.workToDo.push(this.workToDo4);
  //   // }
  //   // else if(this.workToDo1=='undefined' && this.workToDo3=='undefined'){
  //   //   this.workToDo.push(this.workToDo2);
  //   //   this.workToDo.push(this.workToDo4);
  //   // }
  //   // else if(this.workToDo1=='undefined' && this.workToDo4=='undefined'){
  //   //   this.workToDo.push(this.workToDo3);
  //   //   this.workToDo.push(this.workToDo2);
  //   // }
  //   // else if(this.workToDo2=='undefined' && this.workToDo3=='undefined'){
  //   //   this.workToDo.push(this.workToDo1);
  //   //   this.workToDo.push(this.workToDo4);
  //   // }
  //   // else if(this.workToDo2=='undefined' && this.workToDo4=='undefined'){
  //   //   this.workToDo.push(this.workToDo3);
  //   //   this.workToDo.push(this.workToDo1);
  //   // }
  //   // else if(this.workToDo3=='undefined' && this.workToDo4=='undefined'){
  //   //   this.workToDo.push(this.workToDo1);
  //   //   this.workToDo.push(this.workToDo2);
  //   // }
  //   // else if(this.workToDo3=='undefined' && this.workToDo4=='undefined' && this.workToDo1=='undefined'){
  //   //   this.workToDo.push(this.workToDo2);
  //   // }
  //   // else if(this.workToDo3=='undefined' && this.workToDo4=='undefined' && this.workToDo2=='undefined'){
  //   //   this.workToDo.push(this.workToDo1);
  //   // }
  //   // else if(this.workToDo2=='undefined' && this.workToDo4=='undefined' && this.workToDo1=='undefined'){
  //   //   this.workToDo.push(this.workToDo3);
  //   // }
  //   // else if(this.workToDo2=='undefined' && this.workToDo1=='undefined' && this.workToDo3=='undefined'){
  //   //   this.workToDo.push(this.workToDo4);
  //   // }


  //   // console.log("workToDo",this.workToDo);
  //   this.userServicesProvider.getcategoryByValue(this.Category).then((res: any) => {
  //     // console.log("res",res);
  //     // console.log("reslength",res.length);

  //     if (res.length == 0) {
  //       this.userServicesProvider.addCategoryForProject().then((response: any) => {
  //         // alert("fdd");
  //         // console.log("res",response);
  //         response.set({
  //           Category: this.Category,
  //           workToDo: this.workToDo1
  //         })

  //       })
  //       this.presentToastfornew(this.Category);
  //       this.navCtrl.setRoot('AdminMainPage');

  //     }
  //     else {
  //       this.presentToast(this.Category);
  //     }
  //   })

  //   // addDesignationForUser
  // }

  presentToast(desig) {
    const toast = this.toastCtrl.create({
      message: 'category  ' + desig + ' already there',
      duration: 3000
    });
    toast.present();
  }

  presentToastfornew(desig) {
    const toast = this.toastCtrl.create({
      message: 'Category' + desig + ' added successfully',
      duration: 3000
    });
    toast.present();
  }


}
