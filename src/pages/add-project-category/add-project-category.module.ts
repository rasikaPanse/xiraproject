import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddProjectCategoryPage } from './add-project-category';

@NgModule({
  declarations: [
    AddProjectCategoryPage,
  ],
  imports: [
    IonicPageModule.forChild(AddProjectCategoryPage),
  ],
  exports:[
    AddProjectCategoryPage
  ]
})
export class AddProjectCategoryPageModule {}
