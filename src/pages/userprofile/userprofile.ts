import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserServicesProvider } from '../../providers/user-service/user-services';
import { AdminMainPage } from '../admin-main/admin-main';
import  * as moment from 'moment'; 
import * as _ from 'lodash';
import { TimeformatePipe } from '../../pipes/timeformate/timeformate';

/**
 * Generated class for the UserprofilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-userprofile',
  templateUrl: 'userprofile.html',
})
export class UserprofilePage {
  public userDetails:any;
  public assignedProject:any;

  public projectList:any;
  public projectListforadding:any;
  public userId:any;
  public userTask:any;
  // userId:any;
  timings:any;
  times:any;
  totalJobTime:any;  //total working hours
  totalWorkTime:any;  //total productive hours
  totalLunchTime:any;  //total time for lunch
  totalExtraTime:any;  // extra time for personal work
  totalLunchTime1:any;
  timie1:string;
  showTaskToAdmin:boolean=false;
  showTimingsToAdmin:boolean=true;
  showProjectsToAdmin:boolean=false;
  constructor(public navCtrl: NavController, public navParams: NavParams,public userServicesProvider:UserServicesProvider) {
    this.userDetails=this.navParams.get('userdetails');

    // this.userId=localStorage.getItem('userId');
    console.log("this.userDetails",this.userDetails);
  }

//   getFormatedTime(dateString){
//     var date = new Date(dateString);
//     var hours = date.getHours() > 12 ? date.getHours() - 12 : date.getHours();
//     var am_pm = date.getHours() >= 12 ? "pm" : "am";
//     var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
//     let time = hours + ":" + minutes + " " + am_pm;
//     return time;
//  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserprofilePage');
    this.userServicesProvider.getJobStartTime(this.userDetails.key).then((response:any)=>{
      console.log("response...",response);
      this.timings=response;
      
      this.times=_.orderBy(this.timings, ['key', 'key'], ['desc']);
      for(let i=0;i<response.length;i++){
        this.totalWorkTime=null;
        if(response[i].JobStartTime){ 
          
          console.log("response[i].JobEndTime",response[i]);
          console.log("response[i].JobStartTime",response[i].JobStartTime);

          let diff = moment(response[i].JobEndTime, 'HH:mm:ss').diff(moment(response[i].JobStartTime, 'HH:mm:ss'));
          // console.log("diff", diff);
          let d = moment.duration(diff);
          // console.log("d", d);
          this.totalJobTime = Math.floor(d.asHours()) + moment.utc(diff).format(":mm:ss");

            // let diff = moment(response[i].JobEndTime, 'H:m').diff(moment(response[i].JobStartTime, 'H:m'))
            // let d = moment.duration(diff);
            // this.totalJobTime =Math.floor(d.asHours()) +" hours " + moment.utc(diff).format("mm") + " minutes";
            console.log("totalJobTime",this.totalJobTime);

            let lunchdiff = moment(response[i].LunchEndTime, 'HH:mm:ss').diff(moment(response[i].LunchStartTime, 'HH:mm:ss'));
            // console.log("diff", diff);
            let dif = moment.duration(lunchdiff);
            // console.log("d", d);
            this.totalLunchTime = Math.floor(dif.asHours()) + moment.utc(lunchdiff).format(":mm:ss");

            // this.totalLunchTime = Math.floor(dif.asHours()) + moment.utc(lunchdiff).format(":mm");
            
            // let lunchdiff = moment(response[i].LunchEndTime, 'H:m').diff(moment(response[i].LunchStartTime, 'H:m'))
            // let dif = moment.duration(lunchdiff);
            // this.totalLunchTime1 =Math.floor(dif.asHours()) +" hours " + moment.utc(lunchdiff).format("mm") + " minutes";
            console.log("totalLunchTime",this.totalLunchTime);

            if(response[i].ExtraBreakStart && response[i].ExtraBreakEnd){
              let extraDiff = moment(response[i].ExtraBreakEnd, 'HH:mm:ss').diff(moment(response[i].ExtraBreakStart, 'HH:mm:ss'));
            // console.log("diff", diff);
            let exdif = moment.duration(extraDiff);
            // console.log("d", d);
            this.totalExtraTime = Math.floor(exdif.asHours()) + moment.utc(extraDiff).format(":mm:ss");



            let WorkDiff = moment(this.totalJobTime, 'HH:mm:ss').diff(moment(this.totalLunchTime, 'HH:mm:ss'));
            // console.log("diff", diff);
            let b = moment.duration(WorkDiff);
            this.totalLunchTime1 = Math.floor(b.asHours()) + moment.utc(WorkDiff).format(":mm:ss");

            // this.totalLunchTime1 =Math.floor(b.asHours()) +" hours " + moment.utc(WorkDiff).format("mm") + " minutes";
            

            let toDiff = moment(this.totalLunchTime1, 'HH:mm:ss').diff(moment(this.totalExtraTime, 'HH:mm:ss'));
            // console.log("diff", diff);
            let c = moment.duration(toDiff);
            this.totalWorkTime = Math.floor(c.asHours()) + moment.utc(toDiff).format(":mm:ss");

            // this.totalWorkTime =Math.floor(c.asHours()) +" hours " + moment.utc(toDiff).format("mm") + " minutes";
            console.log("d", this.totalWorkTime);
            // this.totalExtraTime = Math.floor(exdif.asHours()) + moment.utc(extraDiff).format(":mm");
            // let WorkDiff= moment(this.totalJobTime, 'H:m').diff(moment(this.totalLunchTime, 'H:m'))
            // let b = moment.duration(WorkDiff);
            console.log("b",moment.utc(WorkDiff));
            // this.totalWorkTime = Math.floor(b.asHours()) + moment.utc(WorkDiff).format(":mm");

            // this.totalWorkTime =Math.floor(b.asHours()) +" hours " + moment.utc(WorkDiff).format("mm") + " minutes";
            console.log("totalLunchTime",this.totalWorkTime);
            response[i].totaljob=this.totalWorkTime;

            }else{
              let WorkDiff = moment(this.totalJobTime, 'HH:mm:ss').diff(moment(this.totalLunchTime, 'HH:mm:ss'));
              // console.log("diff", diff);
              let b = moment.duration(WorkDiff);
              this.totalWorkTime = Math.floor(b.asHours()) + moment.utc(WorkDiff).format(":mm:ss");
              response[i].totaljob=this.totalWorkTime
              // this.totalWorkTime =Math.floor(b.asHours()) +" hours " + moment.utc(WorkDiff).format("mm") + " minutes";
              // this.totalLunchTime=this.totalLunchTime;
            }


            // let WorkDiff= moment(this.totalJobTime, 'H:m').diff(moment(this.totalLunchTime, 'H:m'))
            // let b = moment.duration(WorkDiff);
            // console.log("b",moment.utc(WorkDiff))
            // this.totalWorkTime =Math.floor(b.asHours()) +" hours " + moment.utc(WorkDiff).format("mm") + " minutes";
            // console.log("totalLunchTime",this.totalWorkTime);
            // response[i].totaljob=this.totalWorkTime;
      
        }else{

        }
        


        // this.totalJobTime=(response[i].JobEndTime)-(response[i].JobStartTime);
        // console.log("totalJobTime",this.totalJobTime);
        // console.log("jijhuin",typeof(this.totalJobTime));

      }

    })
    // console.log('ionViewDidLoad UserprofilePage');
    this.userServicesProvider.getProjectforUser().then((projectlistfromdb:any)=>{
      console.log("helo",projectlistfromdb);
      this.projectList=projectlistfromdb;

    })
    console.log("this.userid",this.userId);
    this.userServicesProvider.getAllTaskFromDb(this.userDetails.key).then((resp:any)=>{
      // alert("k")
      console.log("resp",resp);
      this.userTask=resp;
    })
  }

  ionViewWillEnter(){
    // this.userDetails
  }

  showTask(){
    this.showTaskToAdmin=true;
    this.showTimingsToAdmin=false;
    this.showProjectsToAdmin=false;

  }
  showTimings(){
    this.showTimingsToAdmin=true;
    this.showTaskToAdmin=false;
    this.showProjectsToAdmin=false;
  }
  showProjects(){
    this.showProjectsToAdmin=true;
    this.showTimingsToAdmin=false;
    this.showTaskToAdmin=false;
  }

  assignTask(){
    // console.log('uzer',user1)
    this.navCtrl.push('AssignTaskPage',{userDetails:this.userDetails});
  }

  assign(){
  
      this.navCtrl.setRoot(AdminMainPage)
    
  }

}
