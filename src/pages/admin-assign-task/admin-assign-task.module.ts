import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminAssignTaskPage } from './admin-assign-task';

@NgModule({
  declarations: [
    AdminAssignTaskPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminAssignTaskPage),
  ],
  exports:[
    AdminAssignTaskPage
  ]
})
export class AdminAssignTaskPageModule {}
