import { Component } from '@angular/core';
import { UserServicesProvider } from '../../providers/user-service/user-services';

import { IonicPage,NavController,NavParams, AlertController,ViewController  } from 'ionic-angular';
// import { AddTaskPage } from '../add-task/add-task';
import firebase, { database } from 'firebase';
import * as moment from 'moment';
import { ActionSheetController } from 'ionic-angular';
import { DetailedTaskPage } from '../detailed-task/detailed-task';
import { HttpClient } from '@angular/common/http/';
import { HttpHeaders } from '@angular/common/http';
import { AssignTaskPage } from '../assign-task/assign-task';

/**
 * Generated class for the AdminAssignTaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-assign-task',
  templateUrl: 'admin-assign-task.html',
})
export class AdminAssignTaskPage {
  public userId: any;
  public taskList: any;
  public CurrentTime: any;
  public companyName: any;
  public userProfile:any;
  newtimetaken:any;
  public todayDate = moment().format("MM-DD-YYYY");
  constructor(private viewCtrl: ViewController,public alertCtrl: AlertController, private http: HttpClient, public navCtrl: NavController, public userServicesProvider: UserServicesProvider, public actionSheetCtrl: ActionSheetController) {
    this.userId = localStorage.getItem("userId");
    this.companyName = localStorage.getItem('companyName');
  }

  ionViewDidLoad() {
    // alert("h");
    // this.userServicesProvider.getAllTaskFromDb(this.userId).then((response:any)=>{
    //   // console.log("response",response);
    //   this.taskList=response;
    // })
  }

  ionViewWillEnter() {
    // alert("l");
    // this.userServicesProvider.getAllTaskFromDb(this.userId).then((response: any) => {
    //   // console.log("response", response);
    //   this.taskList = response;
    // })
    this.userServicesProvider.getTaskbyTaskStatus(this.userId,'given').then((response:any)=>{
      console.log("rsponse",response);
      this.userServicesProvider.getTaskbyTaskStatus(this.userId,'started').then((res:any)=>{
        console.log("rsponse",res);
        if(res.length==0){

        }else{
          response.push(res[0]);

        }
        console.log("res0000000000",response)
      }).catch((err:any)=>{
        
      })
      this.taskList=response;
      
    })
    
    
    

    this.userServicesProvider.getUserOnAdminSide().then((result:any)=>{
      // console.log("result",result);
      this.userProfile=result;
    })
  }
  addTask() {
    this.navCtrl.push('AddTaskPage');
  }

  startTask(task) {
    // alert("start");
    this.CurrentTime = moment().format("HH:mm");
    // console.log("CurrentTime", this.CurrentTime);

    // this.todaytime = moment().format("HH:mm");
            // alert("2");
            // console.log('Job Started');
            // firebase.database().ref('XiraInfotech').child('UserProfile').child(this.userId).child('Timings').child(this.todayDate)
            //   .update({
            //     JobStartTime: this.CurrentTime,
            //   });
            //   firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).
            //   update({
            //     userStatus: "online"
            //   });
    // console.log("task", task);
    // console.log("taskName",this.taskName);
    // console.log("this.userId",this.userId);
    // console.log("todayDate", this.todayDate);
    // console.log("CurrentTime", this.CurrentTime);
    this.userServicesProvider.getTaskbyTaskStatus(this.userId, "started").then((response: any) => {
      // console.log("response1", response[0]);
      // alert("2 strat");
      let diff = moment(this.CurrentTime, 'HH:mm').diff(moment(response[0].startTime, 'HH:mm'));
      // console.log("diff", diff);
      let d = moment.duration(diff);
      // console.log("d", d);
      let m = Math.floor(d.asHours()) + moment.utc(diff).format(":mm");
      // console.log("m", m);
      // response[0].update({ taskStatus: "pause" });
      // alert("2 strat");

      firebase.database().ref('XiraInfotech').child('UserProfile').child(this.userId).child('Task').child(response[0].key)
        .update({
          taskStatus: "pause",
          timeTaken: m,
          endTime: this.CurrentTime
        });
    }).catch((error: any) => {console.log(error);
      // alert("3 strat");

    });
    firebase.database().ref('XiraInfotech').child('UserProfile').child(this.userId).child('Task').child(task.key)
      .update({
        taskStatus: "started",
        startDate: this.todayDate,
        startTime: this.CurrentTime
      });
    this.ionViewWillEnter();
    this.ionViewDidLoad();
  }

  detailsTask(task, index) {
    // console.log("index home", index)
    this.navCtrl.push(DetailedTaskPage, { taskdetails: task, index: index,flag:'adminAssign' })
  }
  

  taskcomplete(taskkey) {
    // alert(taskkey);
    this.userServicesProvider.getTaskbyTaskStatus(this.userId, "started").then((response: any) => {
    console.log("response",response);
    if(response.length==0){
      alert("you have not started the project");
    }else{
      this.showPrompt(taskkey);
    }
    }).catch((error: any) => console.log(error));



    // this.sendNotification();
  }

  taskPause(taskkey) {
    // console.log("taskkey",taskkey);
    // alert("press pause");
    this.CurrentTime = moment().format("HH:mm");

    this.userServicesProvider.getTaskbyTaskStatus(this.userId, "started").then((response: any) => {
      // console.log("response1", response[0]);
      // alert("1st step");
    if(response.length==0){
      alert("start you task ");
    }else{
      let diff = moment(this.CurrentTime, 'HH:mm').diff(moment(response[0].startTime, 'HH:mm'));
      // console.log("diff", diff);
      // alert("4");
      let d = moment.duration(diff);
      // console.log("d", d);
      let m = Math.floor(d.asHours()) + moment.utc(diff).format(":mm");
    
      // console.log("m", m);
      // console.log("taskkey.timeTaken",taskkey.timeTaken);
      // response[0].update({ taskStatus: "pause" });
      if(taskkey.timeTaken){
        // var a = "01:00";
        // var b = "00:30";
        // alert("3");
        var a = m;
        var b = taskkey.timeTaken;

        this.newtimetaken=this.addtime(a, b);

        
        // console.log("newtimetake",this.newtimetaken);;
      }else{
        // alert("2");
        this.newtimetaken=m;
      }
      firebase.database().ref("XiraInfotech").child('UserProfile').child(this.userId).child('Task').child(taskkey.key)
        .update({
          taskStatus: "pause",
          timeTaken: this.newtimetaken,
          endTime: this.CurrentTime
        });
    }
     
    }).catch((error: any) => console.log(error))
    this.ionViewWillEnter();
    this.ionViewDidLoad();

  }


 addtime(start_time, end_time) {
  // alert("2nd step");

  var startArr = start_time.replace('hrs', '', start_time).split(':');
  var endArr = end_time.replace('hrs', '', end_time).split(':');

  var d = new Date();
  startArr[0] = (startArr[0]) ? parseInt(startArr[0], 10) : 0;
  startArr[1] = (startArr[1]) ? parseInt(startArr[1], 10) : 0;
  endArr[0] = (endArr[0]) ? parseInt(endArr[0], 10) : 0;
  endArr[1] = (endArr[1]) ? parseInt(endArr[1], 10) : 0;

  d.setHours(startArr[0] + endArr[0]);
  d.setMinutes(startArr[1] + endArr[1]);

  var hours = d.getHours();
  var minutes = d.getMinutes();

  return hours + ':' + minutes;
}

  transferTask(taskey){
    // console.log("taskqqq",taskey);
    this.TransferTaskToUser(taskey);
  }

  TransferTaskToUser(taskey) {
      let alert = this.alertCtrl.create();
      alert.setTitle('Lightsaber color');
    for (let i=0;i<this.userProfile.length;i++){
      alert.addInput({
        type: 'radio',
        label: this.userProfile[i].firstName,
        value: this.userProfile[i].key,
        checked: false
      });
    }
  
      alert.addButton('Cancel');
      alert.addButton({
        text: 'OK',
        handler: data => {
          // console.log("data",data);
          // console.log("taskey",taskey);
          this.reasonTransfer(data,taskey);    
      
          // this.userServicesProvider.addTaskToDb(data).then((result:any)=>{
          //   result.set({
          //     Priority:taskey.Priority,
          //     ProjectName:taskey.ProjectName,
          //     RemarkByMe:taskey.RemarkByMe,
          //     TaskDescription:taskey.TaskDescription,
          //     TaskName:taskey.TaskName,
          //     assignedBy:taskey.assignedBy,
          //     assignedDate:taskey.assignedDate,
          //     assignedTime:taskey.assignedTime,
          //     taskStatus:taskey.taskStatus,
          //     taskTransferedBy:data,
              
          //   })
          // })
          // this.testRadioOpen = false;
          // this.testRadioResult = data;
        }
      });
      alert.present();
  }

  reasonTransfer(dataname,taskey){
    // console.log("data",dataname);
    // console.log("task",taskey.userId)
      const prompt = this.alertCtrl.create({
        title: 'Specify Reason',
        message: "Please specify reason for transfer this task",
        inputs: [
          {
            name: 'Reason',
            placeholder: 'Reason'
          },
        ],
        buttons: [
          {
            text: 'Cancel',
            handler: data => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Save',
            handler: data => {
              console.log('Saved clicked');
              // console.log("taskey.key",taskey);
              // this.userServicesProvider.getUserById(taskey.userId).then((respp:any)=>{
              //   console.log("resp",respp);
              // })

              this.userServicesProvider.addTaskToDb(dataname).then((result:any)=>{
                result.set({
                  Priority:taskey.Priority,
                  ProjectName:taskey.ProjectName,
                  TaskDescription:taskey.TaskDescription,
                  TaskName:taskey.TaskName,
                  assignedBy:taskey.assignedBy,
                  assignedDate:taskey.assignedDate,
                  assignedTime:taskey.assignedTime,
                  taskStatus:taskey.taskStatus,
                  taskTransferedBy:dataname,
                  taskTransferedReason:data.Reason
                  
                })
              })
            }
          }
        ]
      });
      prompt.present();
    
  }

  showPrompt(taskkey) {
    const prompt = this.alertCtrl.create({
      title: 'Remark',
      message: "Please Enter Remark",
      inputs: [
        {
          name: 'Remark',
          placeholder: 'Remark'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log('Saved clicked', data);
            this.CurrentTime = moment().format("HH:mm");
            // console.log("CurrentTime", taskkey);

            let diff = moment(this.CurrentTime, 'HH:mm').diff(moment(taskkey.startTime, 'HH:mm'));
            // console.log("diff", diff);
            let d = moment.duration(diff);
            // console.log("d", d);
            let m = Math.floor(d.asHours()) + moment.utc(diff).format(":mm");
            // console.log("m", m);
            firebase.database().ref('XiraInfotech').child('UserProfile').child(this.userId).child('Task').child(taskkey.key)
              .update({
                taskStatus: "completed",
                endTime: this.CurrentTime,
                timeTaken: m,
                RemarkByMe: data.Remark
              });
            this.ionViewWillEnter();
            this.ionViewDidLoad();
          }
        }
      ]
    });
    prompt.present();
  }

}
