import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserTypeChoicePage } from './user-type-choice';

@NgModule({
  declarations: [
    UserTypeChoicePage,
  ],
  imports: [
    IonicPageModule.forChild(UserTypeChoicePage),
  ],
  exports:[
    UserTypeChoicePage
  ]
})
export class UserTypeChoicePageModule {}
