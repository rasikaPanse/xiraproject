import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AdminLoginPage } from '../admin-login/admin-login';
import { LoginPage } from '../login/login';
import { AnimationService, AnimationBuilder } from 'css-animator';

/**
 * Generated class for the UserTypeChoicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-type-choice',
  templateUrl: 'user-type-choice.html',
})
export class UserTypeChoicePage {
  @ViewChild('myElement') myElem;
  private animator: AnimationBuilder;
  constructor(public navCtrl: NavController, public navParams: NavParams,public animationService:AnimationService) {
    this.animator = animationService.builder();
  }
  animateElem() {
    // alert("......");
    this.animator.setType('flipInX').show(this.myElem.nativeElement);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserTypeChoicePage');
  }

  goToAdminLogin(){
    this.navCtrl.push(AdminLoginPage);
  }
  goToUserLogin(){
    // alert("lohin")
    this.navCtrl.push(LoginPage);
  }

}
