import { UserServicesProvider } from './../../providers/user-service/user-services';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav, App } from 'ionic-angular';
import firebase  from 'firebase';
import { UserTypeChoicePage } from '../user-type-choice/user-type-choice';
@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {
  rootPage: any;
  pages = [];
  username = '';
  userType:any;
  isSignIn:any;
  UserType:any;
 
  // Reference to the side menus root nav
  @ViewChild(Nav) nav: Nav;
 
  constructor(public navCtrl: NavController, private authProvider: UserServicesProvider, private appCtrl: App) {
    this.userType=localStorage.getItem('type');
    this.isSignIn = localStorage.getItem("user");
    this.UserType = localStorage.getItem("type");
    // console.log("type",this.userType);

  }

  logout(): Promise<void> {
    // console.log("logout .... ...");
    localStorage.removeItem('userEmail');
    localStorage.removeItem('userId');
    localStorage.removeItem('user');
    localStorage.removeItem('type');
    localStorage.removeItem('companyName');

    // this.app.getRootNav().setRoot(LoginPage);
    this.navCtrl.setRoot(UserTypeChoicePage);
    // this.auth.logout();
    return firebase.auth().signOut();
  }


  // else {
  //   alert("123");
  //   this.rootPage = UserTypeChoicePage;
   
  // }
 
  ionViewWillEnter() {
    this.userType=localStorage.getItem('type');
    // console.log("this.authProvider.isAdmin()",this.userType=='admin');
    
    if (this.isSignIn == 'signIn') {
      // alert("12345");
      if(this.UserType=="admin"){
        // alert("hlo");
        this.pages = [
          { title: 'Admin Dashboard', page: 'AdminMainPage', icon: 'home' },
          { title: 'Task In Review', page: 'ReviewTaskPage', icon: 'paper' },
          { title: 'Projects', page:'ProjectWiseReportPage',icon:'clipboard'},
          { title: 'Services',page: 'ServicesPage',icon:'construct'},

          // { title: 'Add Project Category', page: 'AddProjectCategoryPage', icon:'list-box'},
          { title: 'Add Project', page: 'AddProjectPage', icon:'document' },
          { title: 'Add Designation', page: 'AddDesignationPage', icon:'person'},
          { title: 'Employees',page:'UserSecondPage',icon:'people'},
          // { title: 'Timing report', page: "AdminsideTimingsPage",icon:'clock'}
        ];
        this.username = 'Admin';
  
        this.openPage('AdminMainPage');
      }
      if(this.UserType=="user"){
        // this.rootPage =TabsPage
        // alert("ty");
      this.pages = [
        // { title: 'User Dashboard', page: 'UserPage', icon: 'home' },
        // { title: 'timings', page: 'UserTimingDetailsPage', icon: 'planet' },
        { title: 'UserDasboard',page: 'TaskDifferentChoicePage',icon: 'clipboard'},
        { title: 'Timings',page:'UserTimingDetailsPage',icon:'clock'}
      ];
      this.username = 'User';
      this.openPage('TaskDifferentChoicePage');
      }
  
    }
   
    // if (this.userType=='admin') {
     
    
    // } else
    // if(this.userType=='user') {
      
    // }
  }

 
  // logout() {
  //   this.authProvider.logout();
  //   this.appCtrl.getRootNav().setRoot('LoginPage');
  // }
 
  openPage(page) {
    this.nav.setRoot(page);
  }
 
  // ionViewCanEnter() {
  //   return this.authProvider.isLoggedIn();
  // }
 
}
