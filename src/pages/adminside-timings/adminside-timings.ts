import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserServicesProvider } from '../../providers/user-service/user-services';
import  * as moment from 'moment'; 

/**
 * Generated class for the AdminsideTimingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-adminside-timings',
  templateUrl: 'adminside-timings.html',
})
export class AdminsideTimingsPage {

  userId:any;
  timings:any;
  totalJobTime:any;  //total working hours
  totalWorkTime:any;  //total productive hours
  totalLunchTime:any;  //total time for lunch
  totalExtraTime:any;  // extra time for personal work
  constructor(public navCtrl: NavController, public navParams: NavParams, public userServicesProvider:UserServicesProvider) {
    // this.userId=localStorage.getItem('userId');
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad AdminsideTimingsPage');
  }

  ionViewWillEnter(){
    // console.log('ionViewDidLoad UserTimingDetailsPage');
    this.userServicesProvider.getJobStartTime(this.userId).then((response:any)=>{
      // console.log("response...",response);
      this.timings=response;
      for(let i=0;i<response.length;i++){
        this.totalWorkTime=null;
        if(response[i].JobStartTime){
          
          // console.log("response[i].JobEndTime",response[i]);
          // console.log("response[i].JobStartTime",response[i].JobStartTime);


            let diff = moment(response[i].JobEndTime, 'H:m').diff(moment(response[i].JobStartTime, 'H:m'))
            let d = moment.duration(diff);
            this.totalJobTime =Math.floor(d.asHours()) +" hours " + moment.utc(diff).format("mm") + " minutes";
            // console.log("totalJobTime",this.totalJobTime);


            let lunchdiff = moment(response[i].LunchEndTime, 'H:m').diff(moment(response[i].LunchStartTime, 'H:m'))
            let dif = moment.duration(lunchdiff);
            this.totalLunchTime =Math.floor(dif.asHours()) +" hours " + moment.utc(lunchdiff).format("mm") + " minutes";
            // console.log("totalLunchTime",this.totalLunchTime);

            // if(response[i].ExtraBreakStart && response[i].ExtraBreak)


            let WorkDiff= moment(this.totalJobTime, 'H:m').diff(moment(this.totalLunchTime, 'H:m'))
            let b = moment.duration(WorkDiff);
            // console.log("b",moment.utc(WorkDiff))
            this.totalWorkTime =Math.floor(b.asHours()) +" hours " + moment.utc(WorkDiff).format("mm") + " minutes";
            // console.log("totalLunchTime",this.totalWorkTime);
            response[i].totaljob=this.totalWorkTime;
      
        }else{

        }
        


        // this.totalJobTime=(response[i].JobEndTime)-(response[i].JobStartTime);
        // console.log("totalJobTime",this.totalJobTime);
        // console.log("jijhuin",typeof(this.totalJobTime));

      }

    })
  }

}
