import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminsideTimingsPage } from './adminside-timings';

@NgModule({
  declarations: [
    AdminsideTimingsPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminsideTimingsPage),
  ],
})
export class AdminsideTimingsPageModule {}
