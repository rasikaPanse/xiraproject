import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ShowWhen,ToastController } from 'ionic-angular';
import { UserServicesProvider } from '../../providers/user-service/user-services';

/**
 * Generated class for the AddProjectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-project',
  templateUrl: 'add-project.html',
})
export class AddProjectPage {
  project: any;
  projectDescription: any;
  services: any;
  servicefirst:any;

  projectAdd:any=[];
  constructor(public toastCtrl: ToastController,public alertCtrl: AlertController, public userServicesProvider: UserServicesProvider, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad AddProjectPage');
    this.userServicesProvider.getcategory().then((response: any) => {
      // console.log("log",response);
      this.services = response;
    })
  }

  addProject() {
    this.presentToast();
    this.navCtrl.setRoot('AdminMainPage');

    // console.log("project",this.project);
    // console.log("projectDescription",this.projectDescription);
    // this.showCheckbox(this.project, this.projectDescription);
    // this.userServicesProvider.addProjectsForUser().then((response:any)=>{
    //   response.set({
    //     projectName:this.project,
    //     projectDescription:this.projectDescription,
    //     key:response.key
    //   })
    //   this.navCtrl.setRoot('AdminMainPage');
    // })


    // this.projectListforadding.set({
    //   projectName:this.project,
    // })

  }


  presentToast() {
    const toast = this.toastCtrl.create({
      message: 'Project added successfully',
      duration: 3000
    });
    toast.present();
  }

  showCheckbox(name, category,service) {
    console.log("service",service);
  
        console.log("projectTask",this.projectAdd);

        
        let alert = this.alertCtrl.create();
        alert.setTitle('services');
        for (let i = 0; i < category.length; i++) {
          console.log("");
          alert.addInput({
            type: 'checkbox',
            label: category[i].Category,
            value: category[i].Category,
            checked: false
          });
        }
  
  
        alert.addButton('Cancel');
        alert.addButton({
          text: 'Okay',
          handler: data => {
            console.log('Checkbox data:', data);
            // this.testCheckboxOpen = false;
            // this.testCheckboxResult = data;
            
            this.userServicesProvider.addProjectsForUser().then((response: any) => {
              response.set({
                projectName: this.project,
                category: service,
                projectDescription: data,
                key: response.key
              })
            })
  

            // this.projectListforadding.set({
            //   projectName:this.project,
            // })
  
          }
        });
        alert.present();
      
    }
    
      ///
    

    
    // this.userServicesProvider.getcategoryByValue(category).then((response: any) => {
    //   console.log("response", response[0].workToDo.length);
    //   let alert = this.alertCtrl.create();
    //   alert.setTitle('services');
    //   for (let i = 0; i < response[0].workToDo.length; i++) {
    //     console.log("");
    //     alert.addInput({
    //       type: 'checkbox',
    //       label: response[0].workToDo[i],
    //       value: response[0].workToDo[i],
    //       checked: false
    //     });
    //   }


    //   alert.addButton('Cancel');
    //   alert.addButton({
    //     text: 'Okay',
    //
  

  getProject(two) {
    console.log("ionchange",two);

    console.log('project',this.project);
    for(let j=0;j<two.length;j++){
      this.userServicesProvider.getcategoryByValue(two[j]).then((response: any) => {
        console.log("response", response[0].workToDo.length);
        for(let i=0;i<response[0].workToDo.length;i++){
          let respo = response[0].workToDo[i];
          this.projectAdd.push(response[0].workToDo[i]);

          // respo.map((resp:any)=>{
          //   // alert("3");
          //   console.log("respnse.......",resp);
          //   // response.userId=res[i].key;
          //   // resp.username=response[j].firstName;
          //   // this.projectAdd()
          //   console.log("projectTask123",this.projectAdd);
  
          //  //  this.completedTask.push({userId:res[i].key})
          // })
        }
      
      })
    }
    setTimeout(() => {
      console.log("jbfjsd",this.projectAdd);
    this.showCheckbox(this.project, this.projectAdd,two);

    }, 1000);

    // two is available here
    // but i need the value from first select (one) here as well 
  }


  // addProject

}
