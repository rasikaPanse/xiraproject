import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailedTaskPage } from './detailed-task';

@NgModule({
  declarations: [
    DetailedTaskPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailedTaskPage),
  ],
  exports:[
    DetailedTaskPage
  ]
})
export class DetailedTaskPageModule {}
