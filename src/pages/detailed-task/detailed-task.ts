import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController,App,AlertController } from 'ionic-angular';
import firebase from 'firebase';
import * as moment from 'moment';
import { UserServicesProvider } from '../../providers/user-service/user-services';
/**
 * Generated class for the DetailedTaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detailed-task',
  templateUrl: 'detailed-task.html',
})
export class DetailedTaskPage {

  public taskdetails:any;
  public index:any;
  public pageName:any;
  public CurrentTime;
  public userId:any;
  public newtimetaken;
  public userProfile:any;
  public todayDate = moment().format("MM-DD-YYYY");

  
  constructor(public navCtrl: NavController,public alertCtrl:AlertController, public navParams: NavParams,public viewCtrl: ViewController,public appCtrl: App,
    public userServicesProvider:UserServicesProvider) {
    this.taskdetails=this.navParams.get('taskdetails');
    this.index=this.navParams.get('index');
    this.pageName=this.navParams.get('flag');
    this.userId=localStorage.getItem('userId');
    console.log("taskdetails",this.taskdetails);
    // console.log("index",this.index);
     console.log("flag",this.pageName);


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailedTaskPage');
  }

  ionViewDidLeave(){
    this.viewCtrl.dismiss().catch((err)=>{console.log("not done",err)});
  }


  ionViewWillEnter(){
    this.userServicesProvider.getUserOnAdminSide().then((result:any)=>{
      // console.log("result",result);
      this.userProfile=result;
    })
  }

  // done(){
  //   // console.log("task done",task);
  //   firebase.database().ref('XiraInfotech').child('UserProfile').child(this.taskdetails.userId).child('Task').child(this.taskdetails.key)
  //       .update({
  //         taskStatus: "done",
  //       });

  // }
  // pending(){
  //   // console.log("task",task);
  //   this.showPrompt();
  // }
// for admin assign task function
  showPrompt() {
    const prompt = this.alertCtrl.create({
      title: 'Remark',
      message: "Please Enter Remark",
      inputs: [
        {
          name: 'Remark',
          placeholder: 'Remark'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log('Saved clicked', data);
            // this.CurrentTime = moment().format("HH:mm");
            // console.log("CurrentTime", taskkey);

            // let diff = moment(this.CurrentTime, 'HH:mm').diff(moment(taskkey.startTime, 'HH:mm'));
            // console.log("diff", diff);
            // let d = moment.duration(diff);
            // console.log("d", d);
            // let m = Math.floor(d.asHours()) + moment.utc(diff).format(":mm");
            // console.log("m", m);
            // console.log("companyName",this.companyName)
            firebase.database().ref('XiraInfotech').child('UserProfile').child(this.taskdetails.userId).child('Task').child(this.taskdetails.key).update({
               
                taskStatus: "pending",
                RemarkByAdmin: data.Remark
              });
              // this.ionViewWillEnter();
              this.ionViewDidLoad();
              console.log("kl");
            
          }
        }
      ]
    });
    
    prompt.present();
  }

  taskcomplete() {
    // alert(taskkey);
    this.showPrompt1();



    // this.sendNotification();
  }

  taskPause() {
    // console.log("taskkey",taskkey);
    // alert("press pause");
    this.CurrentTime = moment().format("HH:mm");

    this.userServicesProvider.getTaskbyTaskStatus(this.userId, "started").then((response: any) => {
      // console.log("response1", response[0]);
      // alert("1st step");
      let diff = moment(this.CurrentTime, 'HH:mm').diff(moment(response[0].startTime, 'HH:mm'));
      // console.log("diff", diff);
      // alert("4");
      let d = moment.duration(diff);
      // console.log("d", d);
      let m = Math.floor(d.asHours()) + moment.utc(diff).format(":mm");
    
      // console.log("m", m);
      // console.log("taskkey.timeTaken",taskkey.timeTaken);
      // response[0].update({ taskStatus: "pause" });
      if(this.taskdetails.timeTaken){
        // var a = "01:00";
        // var b = "00:30";
        // alert("3");
        var a = m;
        var b = this.taskdetails.timeTaken;

        this.newtimetaken=this.addtime(a, b);

        
        // console.log("newtimetake",this.newtimetaken);;
      }else{
        // alert("2");
        this.newtimetaken=m;
      }
      firebase.database().ref("XiraInfotech").child('UserProfile').child(this.userId).child('Task').child(this.taskdetails.key)
        .update({
          taskStatus: "pause",
          timeTaken: this.newtimetaken,
          endTime: this.CurrentTime
        });
      // firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).child('Task').child(response[0].key)
      //   .update({
      //     taskStatus: "pause",
      //     timeTaken: m,
      //     endTime: this.CurrentTime
      //   });
    }).catch((error: any) => console.log(error))
    // this.ionViewWillEnter();
    this.ionViewDidLoad();

  }

  startTask() {
    // alert("start");
    this.CurrentTime = moment().format("HH:mm");
    // console.log("CurrentTime", this.CurrentTime);

    // this.todaytime = moment().format("HH:mm");
            // alert("2");
            // console.log('Job Started');
            // firebase.database().ref('XiraInfotech').child('UserProfile').child(this.userId).child('Timings').child(this.todayDate)
            //   .update({
            //     JobStartTime: this.CurrentTime,
            //   });
            //   firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).
            //   update({
            //     userStatus: "online"
            //   });
    // console.log("task", task);
    // console.log("taskName",this.taskName);
    // console.log("this.userId",this.userId);
    // console.log("todayDate", this.todayDate);
    // console.log("CurrentTime", this.CurrentTime);
    this.userServicesProvider.getTaskbyTaskStatus(this.userId, "started").then((response: any) => {
      // console.log("response1", response[0]);
      // alert("2 strat");
      let diff = moment(this.CurrentTime, 'HH:mm').diff(moment(response[0].startTime, 'HH:mm'));
      // console.log("diff", diff);
      let d = moment.duration(diff);
      // console.log("d", d);
      let m = Math.floor(d.asHours()) + moment.utc(diff).format(":mm");
      // console.log("m", m);
      // response[0].update({ taskStatus: "pause" });
      // alert("2 strat");

      firebase.database().ref('XiraInfotech').child('UserProfile').child(this.userId).child('Task').child(response[0].key)
        .update({
          taskStatus: "pause",
          timeTaken: m,
          endTime: this.CurrentTime
        });
    }).catch((error: any) => {console.log(error);
      // alert("3 strat");

    });
    firebase.database().ref('XiraInfotech').child('UserProfile').child(this.taskdetails.userId).child('Task').child(this.taskdetails.key)
      .update({
        taskStatus: "started",
        startDate: this.todayDate,
        startTime: this.CurrentTime
      });
    // this.ionViewWillEnter();
    this.ionViewDidLoad();
  }


  addtime(start_time, end_time) {
    // alert("2nd step");

    var startArr = start_time.replace('hrs', '', start_time).split(':');
    var endArr = end_time.replace('hrs', '', end_time).split(':');

    var d = new Date();
    startArr[0] = (startArr[0]) ? parseInt(startArr[0], 10) : 0;
    startArr[1] = (startArr[1]) ? parseInt(startArr[1], 10) : 0;
    endArr[0] = (endArr[0]) ? parseInt(endArr[0], 10) : 0;
    endArr[1] = (endArr[1]) ? parseInt(endArr[1], 10) : 0;

    d.setHours(startArr[0] + endArr[0]);
    d.setMinutes(startArr[1] + endArr[1]);

    var hours = d.getHours();
    var minutes = d.getMinutes();

    return hours + ':' + minutes;
  }

  transferTask(){
    // console.log("taskqqq",taskey);
    this.TransferTaskToUser();
  }

  TransferTaskToUser() {
      let alert = this.alertCtrl.create();
      alert.setTitle('Lightsaber color');
    for (let i=0;i<this.userProfile.length;i++){
      alert.addInput({
        type: 'radio',
        label: this.userProfile[i].firstName,
        value: this.userProfile[i].key,
        checked: false
      });
    }
  
      alert.addButton('Cancel');
      alert.addButton({
        text: 'OK',
        handler: data => {
          // console.log("data",data);
          // console.log("taskey",taskey);
          this.reasonTransfer(data);    
      
          // this.userServicesProvider.addTaskToDb(data).then((result:any)=>{
          //   result.set({
          //     Priority:taskey.Priority,
          //     ProjectName:taskey.ProjectName,
          //     RemarkByMe:taskey.RemarkByMe,
          //     TaskDescription:taskey.TaskDescription,
          //     TaskName:taskey.TaskName,
          //     assignedBy:taskey.assignedBy,
          //     assignedDate:taskey.assignedDate,
          //     assignedTime:taskey.assignedTime,
          //     taskStatus:taskey.taskStatus,
          //     taskTransferedBy:data,
              
          //   })
          // })
          // this.testRadioOpen = false;
          // this.testRadioResult = data;
        }
      });
      alert.present();
  }

  reasonTransfer(dataname){
    // console.log("data",dataname);
    // console.log("task",taskey.userId)
      const prompt = this.alertCtrl.create({
        title: 'Specify Reason',
        message: "Please specify reason for transfer this task",
        inputs: [
          {
            name: 'Reason',
            placeholder: 'Reason'
          },
        ],
        buttons: [
          {
            text: 'Cancel',
            handler: data => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Save',
            handler: data => {
              console.log('Saved clicked');
              // console.log("taskey.key",taskey);
              // this.userServicesProvider.getUserById(taskey.userId).then((respp:any)=>{
              //   console.log("resp",respp);
              // })

              this.userServicesProvider.addTaskToDb(dataname).then((result:any)=>{
                result.set({
                  Priority:this.taskdetails.Priority,
                  ProjectName:this.taskdetails.ProjectName,
                  TaskDescription:this.taskdetails.TaskDescription,
                  TaskName:this.taskdetails.TaskName,
                  assignedBy:this.taskdetails.assignedBy,
                  assignedDate:this.taskdetails.assignedDate,
                  assignedTime:this.taskdetails.assignedTime,
                  taskStatus:this.taskdetails.taskStatus,
                  taskTransferedBy:dataname,
                  taskTransferedReason:data.Reason
                  
                })
              })
            }
          }
        ]
      });
      prompt.present();
    
  }

  showPrompt1() {
    const prompt = this.alertCtrl.create({
      title: 'Remark',
      message: "Please Enter Remark",
      inputs: [
        {
          name: 'Remark',
          placeholder: 'Remark'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log('Saved clicked', data);
            this.CurrentTime = moment().format("HH:mm");
            // console.log("CurrentTime", taskkey);

            let diff = moment(this.CurrentTime, 'HH:mm').diff(moment(this.taskdetails.startTime, 'HH:mm'));
            // console.log("diff", diff);
            let d = moment.duration(diff);
            // console.log("d", d);
            let m = Math.floor(d.asHours()) + moment.utc(diff).format(":mm");
            // console.log("m", m);
            firebase.database().ref('XiraInfotech').child('UserProfile').child(this.userId).child('Task').child(this.taskdetails.key)
              .update({
                taskStatus: "completed",
                endTime: this.CurrentTime,
                timeTaken: m,
                RemarkByMe: data.Remark
              });
            // this.ionViewWillEnter();
            this.ionViewDidLoad();
          }
        }
      ]
    });
    prompt.present();
  }
  // finish admin assign task 


  //pending task
  startTaskPending() {
    this.CurrentTime = moment().format("HH:mm");
    console.log("CurrentTime", this.CurrentTime);
    // console.log("task", task);
    // console.log("taskName",this.taskName);
    // console.log("this.userId",this.userId);
    console.log("todayDate", this.todayDate);
    console.log("CurrentTime", this.CurrentTime);
    this.userServicesProvider.getTaskbyTaskStatus(this.userId, "started").then((response: any) => {
      console.log("response1", response[0]);

      let diff = moment(this.CurrentTime, 'HH:mm').diff(moment(response[0].startTime, 'HH:mm'));
      console.log("diff", diff);
      let d = moment.duration(diff);
      console.log("d", d);
      let m = Math.floor(d.asHours()) + moment.utc(diff).format(":mm");
      console.log("m", m);

      this.userServicesProvider.getTaskbyTaskStatus(this.userId, "pause").then((res: any) => {
        console.log("response1", res);
      })

      // response[0].update({ taskStatus: "pause" });
      firebase.database().ref('XiraInfotech').child('UserProfile').child(this.userId).child('Task').child(response[0].key)
        .update({
          taskStatus: "pause",
          timeTaken: m,
          endTime: this.CurrentTime
        });
    }).catch((error: any) => console.log(error));

    firebase.database().ref('XiraInfotech').child('UserProfile').child(this.userId).child('Task').child(this.taskdetails.key)
      .update({
        taskStatus: "started",
        startDate: this.todayDate,
        startTime: this.CurrentTime
      });
    this.ionViewWillEnter();
    this.ionViewDidLoad();
  }

  taskcompletepending() {
    // alert(taskkey);
    this.showPromptpendng();



    // this.sendNotification();
  }

  taskPausepending() {
    // alert(taskkey);
    // console.log("taskkey",taskkey);
    // console.log("133");
    // alert("2");
    // console.log("timeTaken",taskkey.timeTaken);

    this.userServicesProvider.getTaskbyTaskStatus(this.userId, "started").then((response: any) => {
      // console.log("response1", response[0]);

      let diff = moment(this.CurrentTime, 'HH:mm').diff(moment(response[0].startTime, 'HH:mm'));
      // console.log("diff", diff);
      let d = moment.duration(diff);
      // console.log("d", d);
      let m = Math.floor(d.asHours()) + moment.utc(diff).format(":mm");
      // console.log("m", m);
      response[0].update({ taskStatus: "pause" });
      
      firebase.database().ref('XiraInfotech').child('UserProfile').child(this.userId).child('Task').child(this.taskdetails.key)
        .update({
          taskStatus: "pause",
          timeTaken: m,
          endTime: this.CurrentTime
        });
      // firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).child('Task').child(response[0].key)
      //   .update({
      //     taskStatus: "pause",
      //     timeTaken: m,
      //     endTime: this.CurrentTime
      //   });
    }).catch((error: any) => console.log(error))
    this.ionViewWillEnter();
    this.ionViewDidLoad();

  }

  transferTaskPending(){
    // console.log("taskqqq",taskey);
    this.TransferTaskToUserpending();
  }

  TransferTaskToUserpending() {
      let alert = this.alertCtrl.create();
      alert.setTitle('Lightsaber color');
    for (let i=0;i<this.userProfile.length;i++){
      alert.addInput({
        type: 'radio',
        label: this.userProfile[i].firstName,
        value: this.userProfile[i].key,
        checked: false
      });
    }
  
      alert.addButton('Cancel');
      alert.addButton({
        text: 'OK',
        handler: data => {
          // console.log("data",data);
          // console.log("taskey",taskey);
          this.reasonTransferpending(data);    
      
          // this.userServicesProvider.addTaskToDb(data).then((result:any)=>{
          //   result.set({
          //     Priority:taskey.Priority,
          //     ProjectName:taskey.ProjectName,
          //     RemarkByMe:taskey.RemarkByMe,
          //     TaskDescription:taskey.TaskDescription,
          //     TaskName:taskey.TaskName,
          //     assignedBy:taskey.assignedBy,
          //     assignedDate:taskey.assignedDate,
          //     assignedTime:taskey.assignedTime,
          //     taskStatus:taskey.taskStatus,
          //     taskTransferedBy:data,
              
          //   })
          // })
          // this.testRadioOpen = false;
          // this.testRadioResult = data;
        }
      });
      alert.present();
  }

  reasonTransferpending(dataname){
    // console.log("data",dataname);
    // console.log("task",taskey)
      const prompt = this.alertCtrl.create({
        title: 'Specify Reason',
        message: "Please specify reason for transfer this task",
        inputs: [
          {
            name: 'Reason',
            placeholder: 'Reason'
          },
        ],
        buttons: [
          {
            text: 'Cancel',
            handler: data => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Save',
            handler: data => {
              console.log('Saved clicked');
              this.userServicesProvider.addTaskToDb(dataname).then((result:any)=>{
                result.set({
                  Priority:this.taskdetails.Priority,
                  ProjectName:this.taskdetails.ProjectName,
                  TaskDescription:this.taskdetails.TaskDescription,
                  TaskName:this.taskdetails.TaskName,
                  assignedBy:this.taskdetails.assignedBy,
                  assignedDate:this.taskdetails.assignedDate,
                  assignedTime:this.taskdetails.assignedTime,
                  taskStatus:this.taskdetails.taskStatus,
                  taskTransferedBy:dataname,
                  taskTransferedReason:data.reason
                  
                })
              })
            }
          }
        ]
      });
      prompt.present();
    
  }

  showPromptpendng() {
    const prompt = this.alertCtrl.create({
      title: 'Remark',
      message: "Please Enter Remark",
      inputs: [
        {
          name: 'Remark',
          placeholder: 'Remark'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log('Saved clicked', data);
            this.CurrentTime = moment().format("HH:mm");
            // console.log("CurrentTime", taskkey);

            let diff = moment(this.CurrentTime, 'HH:mm').diff(moment(this.taskdetails.startTime, 'HH:mm'));
            // console.log("diff", diff);
            let d = moment.duration(diff);
            // console.log("d", d);
            let m = Math.floor(d.asHours()) + moment.utc(diff).format(":mm");
            // console.log("m", m);
            firebase.database().ref('XiraInfotech').child('UserProfile').child(this.userId).child('Task').child(this.taskdetails.key)
              .update({
                taskStatus: "completed",
                endTime: this.CurrentTime,
                timeTaken: m,
                RemarkByMe: data.Remark
              });
            this.ionViewWillEnter();
            this.ionViewDidLoad();
          }
        }
      ]
    });
    prompt.present();
  }
  // finish pending task

  //review task 
  donereview(){
    // console.log("task done",task);
    firebase.database().ref('XiraInfotech').child('UserProfile').child(this.taskdetails.userId).child('Task').child(this.taskdetails.key)
        .update({
          taskStatus: "done",
        });

        this.navCtrl.setRoot('ReviewTaskPage');

  }

  pendingReview(){
    // console.log("task",task);?
    this.showPromptreview();
  }

  showPromptreview() {
    const prompt = this.alertCtrl.create({
      title: 'Remark',
      message: "Please Enter Remark",
      inputs: [
        {
          name: 'Remark',
          placeholder: 'Remark'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log('Saved clicked', data);
            // this.CurrentTime = moment().format("HH:mm");
            // console.log("CurrentTime", taskkey);

            // let diff = moment(this.CurrentTime, 'HH:mm').diff(moment(taskkey.startTime, 'HH:mm'));
            // console.log("diff", diff);
            // let d = moment.duration(diff);
            // console.log("d", d);
            // let m = Math.floor(d.asHours()) + moment.utc(diff).format(":mm");
            // console.log("m", m);
            // console.log("companyName",this.companyName)
            firebase.database().ref('XiraInfotech').child('UserProfile').child(this.taskdetails.userId).child('Task').child(this.taskdetails.key).update({
               
                taskStatus: "pending",
                RemarkByAdmin: data.Remark
              });
              // this.ionViewWillEnter();
              // this.ionViewDidLoad();
              console.log("kl");
              this.navCtrl.setRoot('ReviewTaskPage');
          }

        }
      ]
    });
    
    prompt.present();
  }
  //finish review task
}
