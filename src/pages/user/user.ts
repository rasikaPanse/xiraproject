import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserServicesProvider } from '../../providers/user-service/user-services'
 /**
 * Generated class for the UserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
})
export class UserPage {
  public itemTask:any;
  public projectTask:any=[];
  public showproject:boolean=false;
  public showTask:boolean=true;
  public check:boolean=true;
  constructor(public navCtrl: NavController, public navParams: NavParams,public userServicesProvider:UserServicesProvider) {
   this.itemTask= this.navParams.get('item');
   console.log("this",this.itemTask);
  }

  ionViewDidLoad() {
    this.projectTask=[];

    console.log('ionViewDidLoad UserPage');
    this.userServicesProvider.getUserOnAdminSide().then((response:any)=>{
      console.log("response",response);
      console.log("length of repsonse",response.length);
      for(let i=0;i<response.length;i++){
        // alert("1");
        this.userServicesProvider.getTaskbyProject(response[i].key,this.itemTask.projectName).then((result:any)=>{
          console.log("result1234",result);
          // alert("6");
          result.map((resp:any)=>{
            // alert("3");
            console.log("respnse.......",resp);
            // response.userId=res[i].key;
            resp.username=response[i].firstName;
            
            this.projectTask.push(resp);
           //  this.completedTask.push({userId:res[i].key})
            console.log("projectTask",this.projectTask);
          })
          
        })
      }
    })
  }


  showProjects(){
    this.showTask=false;
    this.showproject=true;
    this.check=false
  }
  showTasks(){
    this.showTask=true;
    this.showproject=false;
    this.check=false;
  }

  // this.showTimingsToAdmin=true;
  // this.showTaskToAdmin=false;
  // this.showProjectsToAdmin=false;

  

}
