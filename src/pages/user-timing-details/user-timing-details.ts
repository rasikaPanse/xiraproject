import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserServicesProvider } from '../../providers/user-service/user-services';
import  * as moment from 'moment'; 
import * as _ from 'lodash';

 /**
 * Generated class for the UserTimingDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-timing-details',
  templateUrl: 'user-timing-details.html',
})
export class UserTimingDetailsPage {
  userId:any;
  timings:any;
  email:any;
  times:any;
  totalJobTime:any;  //total working hours
  totalWorkTime:any;  //total productive hours
  totalLunchTime:any;  //total time for lunch
  totalLunchTime1:any //
  totalExtraTime:any;  // extra time for personal work
  showTaskToAdmin:boolean=false;
  showTimingsToAdmin:boolean=true;
  showProjectsToAdmin:boolean=false;
  public userTask:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public userServicesProvider:UserServicesProvider) {
    this.userId=localStorage.getItem('userId');
    this.email=localStorage.getItem('userEmail');
  }

  ionViewDidLoad() {
   
    this.userServicesProvider.getJobStartTime(this.userId).then((response:any)=>{
      console.log("response...",response);
      this.timings=response;
      this.times=_.orderBy(this.timings, ['key', 'key'], ['desc']);
      console.log("console",this.times)
      for(let i=0;i<response.length;i++){
        this.totalWorkTime=null;
        if(response[i].JobStartTime){
          
          console.log("response[i].JobEndTime",response[i]);
          console.log("response[i].JobStartTime",response[i].JobStartTime);

          let diff = moment(response[i].JobEndTime, 'HH:mm').diff(moment(response[i].JobStartTime, 'HH:mm'));
          // console.log("diff", diff);
          let d = moment.duration(diff);
          // console.log("d", d);
          this.totalJobTime = Math.floor(d.asHours()) + moment.utc(diff).format(":mm");

            // let diff = moment(response[i].JobEndTime, 'H:m').diff(moment(response[i].JobStartTime, 'H:m'))
            // let d = moment.duration(diff);
            // this.totalJobTime =Math.floor(d.asHours()) +" hours " + moment.utc(diff).format("mm") + " minutes";
            console.log("totalJobTime",this.totalJobTime);

            let lunchdiff = moment(response[i].LunchEndTime, 'HH:mm').diff(moment(response[i].LunchStartTime, 'HH:mm'));
            // console.log("diff", diff);
            let dif = moment.duration(lunchdiff);
            // console.log("d", d);
            this.totalLunchTime = Math.floor(dif.asHours()) + moment.utc(lunchdiff).format(":mm");

            // this.totalLunchTime = Math.floor(dif.asHours()) + moment.utc(lunchdiff).format(":mm");
            
            // let lunchdiff = moment(response[i].LunchEndTime, 'H:m').diff(moment(response[i].LunchStartTime, 'H:m'))
            // let dif = moment.duration(lunchdiff);
            // this.totalLunchTime1 =Math.floor(dif.asHours()) +" hours " + moment.utc(lunchdiff).format("mm") + " minutes";
            console.log("totalLunchTime",this.totalLunchTime);

            if(response[i].ExtraBreakStart && response[i].ExtraBreakEnd){
              let extraDiff = moment(response[i].ExtraBreakEnd, 'HH:mm').diff(moment(response[i].ExtraBreakStart, 'HH:mm'));
            // console.log("diff", diff);
            let exdif = moment.duration(extraDiff);
            // console.log("d", d);
            this.totalExtraTime = Math.floor(exdif.asHours()) + moment.utc(extraDiff).format(":mm");



            let WorkDiff = moment(this.totalJobTime, 'HH:mm').diff(moment(this.totalLunchTime, 'HH:mm'));
            // console.log("diff", diff);
            let b = moment.duration(WorkDiff);
            this.totalLunchTime1 = Math.floor(b.asHours()) + moment.utc(WorkDiff).format(":mm");

            // this.totalLunchTime1 =Math.floor(b.asHours()) +" hours " + moment.utc(WorkDiff).format("mm") + " minutes";
            

            let toDiff = moment(this.totalLunchTime1, 'HH:mm').diff(moment(this.totalExtraTime, 'HH:mm'));
            // console.log("diff", diff);
            let c = moment.duration(toDiff);
            this.totalWorkTime = Math.floor(c.asHours()) + moment.utc(toDiff).format(":mm");

            // this.totalWorkTime =Math.floor(c.asHours()) +" hours " + moment.utc(toDiff).format("mm") + " minutes";
            console.log("d", this.totalWorkTime);
            // this.totalExtraTime = Math.floor(exdif.asHours()) + moment.utc(extraDiff).format(":mm");
            // let WorkDiff= moment(this.totalJobTime, 'H:m').diff(moment(this.totalLunchTime, 'H:m'))
            // let b = moment.duration(WorkDiff);
            console.log("b",moment.utc(WorkDiff));
            // this.totalWorkTime = Math.floor(b.asHours()) + moment.utc(WorkDiff).format(":mm");

            // this.totalWorkTime =Math.floor(b.asHours()) +" hours " + moment.utc(WorkDiff).format("mm") + " minutes";
            console.log("totalLunchTime",this.totalWorkTime);
            response[i].totaljob=this.totalWorkTime;

            }else{
              let WorkDiff = moment(this.totalJobTime, 'HH:mm').diff(moment(this.totalLunchTime, 'HH:mm'));
              // console.log("diff", diff);
              let b = moment.duration(WorkDiff);
              this.totalWorkTime = Math.floor(b.asHours()) + moment.utc(WorkDiff).format(":mm");
              response[i].totaljob=this.totalWorkTime

              // this.totalWorkTime =Math.floor(b.asHours()) +" hours " + moment.utc(WorkDiff).format("mm") + " minutes";
              // this.totalLunchTime=this.totalLunchTime;
            }


            // let WorkDiff= moment(this.totalJobTime, 'H:m').diff(moment(this.totalLunchTime, 'H:m'))
            // let b = moment.duration(WorkDiff);
            // console.log("b",moment.utc(WorkDiff))
            // this.totalWorkTime =Math.floor(b.asHours()) +" hours " + moment.utc(WorkDiff).format("mm") + " minutes";
            // console.log("totalLunchTime",this.totalWorkTime);
            // response[i].totaljob=this.totalWorkTime;
      
        }else{

        }
        


        // this.totalJobTime=(response[i].JobEndTime)-(response[i].JobStartTime);
        // console.log("totalJobTime",this.totalJobTime);
        // console.log("jijhuin",typeof(this.totalJobTime));

      }

    })

    this.userServicesProvider.getAllTaskFromDb(this.userId).then((resp:any)=>{
      // alert("k")
      console.log("resp",resp);
      this.userTask=resp;
    })
    //     // alert("1");
    //     // this.userServicesProvider.getJobStartTime(respon[i].key).then((response:any)=>{
    //     //   console.log("response...",response);
    //     //   this.timings=response;
    //     //   for(let i=0;i<response.length;i++){
    //     //     this.totalWorkTime=null;
    //     //     if(response[i].JobStartTime){
              
    //     //       console.log("response[i].JobEndTime",response[i]);
    //     //       console.log("response[i].JobStartTime",response[i].JobStartTime);
    
    
    //     //         let diff = moment(response[i].JobEndTime, 'H:m').diff(moment(response[i].JobStartTime, 'H:m'))
    //     //         let d = moment.duration(diff);
    //     //         this.totalJobTime =Math.floor(d.asHours()) +" hours " + moment.utc(diff).format("mm") + " minutes";
    //     //         console.log("totalJobTime",this.totalJobTime);
    
    
    //     //         let lunchdiff = moment(response[i].LunchEndTime, 'H:m').diff(moment(response[i].LunchStartTime, 'H:m'))
    //     //         let dif = moment.duration(lunchdiff);
    //     //         this.totalLunchTime =Math.floor(dif.asHours()) +" hours " + moment.utc(lunchdiff).format("mm") + " minutes";
    //     //         console.log("totalLunchTime",this.totalLunchTime);
    
    //     //         // if(response[i].ExtraBreakStart && response[i].ExtraBreak)
    
    
    //     //         let WorkDiff= moment(this.totalJobTime, 'H:m').diff(moment(this.totalLunchTime, 'H:m'))
    //     //         let b = moment.duration(WorkDiff);
    //     //         console.log("b",moment.utc(WorkDiff))
    //     //         this.totalWorkTime =Math.floor(b.asHours()) +" hours " + moment.utc(WorkDiff).format("mm") + " minutes";
    //     //         console.log("totalLunchTime",this.totalWorkTime);
    //     //         response[i].totaljob=this.totalWorkTime;
          
    //     //     }else{
    
    //     //     }
            
    
    
    //     //     // this.totalJobTime=(response[i].JobEndTime)-(response[i].JobStartTime);
    //     //     // console.log("totalJobTime",this.totalJobTime);
    //     //     // console.log("jijhuin",typeof(this.totalJobTime));
    
    //     //   }
    
    //     // })
      
    
  }

  showTask(){
    this.showTaskToAdmin=true;
    this.showTimingsToAdmin=false;
    this.showProjectsToAdmin=false;

  }
  showTimings(){
    this.showTimingsToAdmin=true;
    this.showTaskToAdmin=false;
    this.showProjectsToAdmin=false;
  }
  showProjects(){
    this.showProjectsToAdmin=true;
    this.showTimingsToAdmin=false;
    this.showTaskToAdmin=false;
  }

  ionViewWillEnter(){
    console.log('ionViewDidLoad UserTimingDetailsPage');
  
  }





}
