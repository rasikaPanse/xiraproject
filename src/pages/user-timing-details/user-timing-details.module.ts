import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserTimingDetailsPage } from './user-timing-details';

@NgModule({
  declarations: [
    UserTimingDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(UserTimingDetailsPage),
  ],
  exports:[
    UserTimingDetailsPage
  ]
})
export class UserTimingDetailsPageModule {}
