import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserServicesProvider } from '../../providers/user-service/user-services';

/**
 * Generated class for the CompletedTaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-completed-task',
  templateUrl: 'completed-task.html',
})
export class CompletedTaskPage {
  public userId:any;
  completedTask:any;
  CurrentTime:any;
  companyName:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public userServicesProvider:UserServicesProvider) {
    this.userId=localStorage.getItem('userId');
  this.companyName='XiraInfotech';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CompletedTaskPage');
  }

  ionViewWillEnter(){
    this.userServicesProvider.getTaskbyTaskStatus(this.userId,'completed').then((response:any)=>{
      // console.log("rsponse",response);
      this.completedTask=response;
    })
  }

  detailsTask(task, index) {
    // console.log("index home", index)
    this.navCtrl.push('DetailedTaskPage', { taskdetails: task, index: index,flag:'completed' })
  }

}
