import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserServicesProvider } from '../../providers/user-service/user-services'
import { FormBuilder, FormArray, FormGroup, Validators } from '@angular/forms';
import * as _ from 'lodash';
import firebase from 'firebase';

/**
 * Generated class for the ServiceEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-service-edit',
  templateUrl: 'service-edit.html',
})
export class ServiceEditPage {
  public form: FormGroup;
  public ServiceList:any;
  public listItem:any;
  public mergeItem:any;
  public value:boolean=false;
  constructor(public navCtrl: NavController,private _FB: FormBuilder, public navParams: NavParams, public userServicesProvider:UserServicesProvider) {
  this.listItem=(navParams.get('items'));
  console.log("listItem",this.listItem);
  this.form = this._FB.group({
    Category: [this.listItem.Category, Validators.required],
    workToDo1: this._FB.array([
      // this.listItem.workToDo,

      this.initTechnologyFields(),
    ])
  });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ServiceEditPage');
    this.userServicesProvider.getService().then((response:any)=>{
      console.log("respi",response);
      this.ServiceList=response
    })
  }
  initTechnologyFields(): FormGroup {
    return this._FB.group({
      Category: ['', Validators.required]
    });
  }
  // initTechnologyFields(): FormGroup {
  //   return this._FB.group({
  //     Category: [JSON.stringify(this.listItem.workToDo) , Validators.required]
  //   });
  // }

  // initTechnologyFields112(): FormGroup {
  //   return this._FB.group({
  //     Category: ['' , Validators.required]
  //   });
  // }

  addNewInputField(): void {
    this.value=true
    const control = <FormArray>this.form.controls.workToDo1;
    // this.removeInputField(0);
    control.push(this.initTechnologyFields());
  }
  addNewInputField1(i): void {
    this.value=true
    const control = <FormArray>this.form.controls.workToDo1;
    this.removeInputField(i);
    control.push(this.initTechnologyFields());
  }
  removeInputField(i: number): void {
    const control = <FormArray>this.form.controls.workToDo1;
    control.removeAt(i);
  }
  manage(val: any,item:any): void {
    console.log("njkkf",val);
    console.log("sdcsd",item);
    this.mergeItem=_.unionBy(val.workToDo1, item.workToDo, 'Category');
    // this.mergeItem=_.merge(val.workToDo1, item.workToDo);
    console.log("djkfsnd",this.mergeItem);
  
        // this.userServicesProvider.addCategoryForProject().then((response: any) => {
        //   // alert("fdd");
        //   // console.log("res",response);
        //   response.set({
        //     Category: item.Category,
        //     workToDo: val.mergeItem
        //   })

        // })
        firebase.database().ref('XiraInfotech').child('ProjectCategory').child(item.key).
        update({
          Category: item.Category,
            workToDo: this.mergeItem
        });
        this.navCtrl.setRoot('ServicesPage');
      
    
  }
  manage1(item:any): void {
    // console.log("njkkf",val);
    console.log("sdcsd",item);
    // this.mergeItem=_.unionBy(val.workToDo1, item.workToDo, 'Category');
    // this.mergeItem=_.merge(val.workToDo1, item.workToDo);
    console.log("djkfsnd",this.mergeItem);
  
        // this.userServicesProvider.addCategoryForProject().then((response: any) => {
        //   // alert("fdd");
        //   // console.log("res",response);
        //   response.set({
        //     Category: item.Category,
        //     workToDo: val.mergeItem
        //   })

        // })
        firebase.database().ref('XiraInfotech').child('ProjectCategory').child(item.key).
        update({
          Category: item.Category,
            workToDo: item.workToDo
        });
        this.navCtrl.setRoot('ServicesPage');
      
    
  }

}
