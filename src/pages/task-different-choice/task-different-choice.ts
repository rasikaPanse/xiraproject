import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, ViewController } from 'ionic-angular';
import { UserServicesProvider } from '../../providers/user-service/user-services';
import * as moment from 'moment';
import { PendingTaskPage } from '../pending-task/pending-task';
import { AdminAssignTaskPage } from '../admin-assign-task/admin-assign-task';
import { CompletedTaskPage } from '../completed-task/completed-task';
import firebase from 'firebase';
/**
* Generated class for the TaskDifferentChoicePage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@IonicPage()
@Component({
  selector: 'page-task-different-choice',
  templateUrl: 'task-different-choice.html',
})
export class TaskDifferentChoicePage {
  companyName: any;
  userId: any;
  // jobStatus:any;
  public isToggled: boolean;
  public todayDate = moment().format("MM-DD-YYYY");
  public todaytime: any;
  public value:any;
  public value1:any;
  constructor(private viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams, public userServicesProvider: UserServicesProvider, public actionSheetCtrl: ActionSheetController) {
    this.companyName = 'XiraInfotech';
    this.userId = localStorage.getItem('userId');
    this.value1=localStorage.getItem('isToggled');
    console.log("]dfsdf",this.value1)
    console.log("isToggled",this.value1);
    if(this.value1== null ){
      this.isToggled = false;
    }else{
      if(this.value1='true'){
        this.isToggled = true;
      }else{
        this.isToggled = false;

      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TaskDifferentChoicePage');
  }
  ionViewWillEnter() {
    this.viewCtrl.showBackButton(false);

  }
  public notify() {
    console.log("Toggled: " + this.isToggled);
  
    if(this.isToggled){
      this.value='true';
      localStorage.setItem("isToggled",this.value);
      this.todaytime = moment().format("HH:mm");
            // alert("2");
            // console.log('Job Started');
            this.userServicesProvider.getTimingsByDate(this.userId, this.todayDate).then((response: any) => {
              // console.log("response",response[0].JobStartTime);
              if (response.length != 0) {
                if (response[0].JobStartTime == 'undefined') {
                  firebase.database().ref('XiraInfotech').child('UserProfile').child(this.userId).child('Timings').child(this.todayDate)
                    .update({
                      JobStartTime: this.todaytime,
                      key: this.todayDate
                    });
                  firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).
                    update({
                      userStatus: "online"
                    });
                } else {
                  alert("already added")
                }
              }
              else {
                firebase.database().ref('XiraInfotech').child('UserProfile').child(this.userId).child('Timings').child(this.todayDate)
                  .update({
                    JobStartTime: this.todaytime,
                    key: this.todayDate
                  });
                firebase.database().ref('XiraInfotech').child('UserProfile').child(this.userId).
                  update({
                    userStatus: "online"
                  });
              }

            })
    }else{
      this.todaytime = moment().format("HH:mm");
      // alert("2");
      // console.log('Job Started');
      localStorage.removeItem("isToggled");
      this.value='false';
      this.userServicesProvider.getTimingsByDate(this.userId, this.todayDate).then((response: any) => {
        // console.log("response",response[0].JobStartTime);
        if (response.length != 0) {
          if (response[0].JobStartTime == 'undefined') {
            firebase.database().ref('XiraInfotech').child('UserProfile').child(this.userId).child('Timings').child(this.todayDate)
              .update({
                JobEndTime: this.todaytime,
                key: this.todayDate
              });
              this.isToggled=false;
            firebase.database().ref('XiraInfotech').child('UserProfile').child(this.userId).
              update({
                userStatus: "offline"
              });
          } else {
            alert("already Added")
          }
        }
        else {
          firebase.database().ref('XiraInfotech').child('UserProfile').child(this.userId).child('Timings').child(this.todayDate)
            .update({
              JobEndTime: this.todaytime,
              key: this.todayDate
            });
          firebase.database().ref('XiraInfotech').child('UserProfile').child(this.userId).
            update({
              userStatus: "offline"
            });
        }

      })
    }
  }

  goToPending() {
    this.navCtrl.push(PendingTaskPage)
    // this.navCtrl.push(PendingTaskPage);
  }
  goToAdminTask() {
    this.navCtrl.push(AdminAssignTaskPage);
  }

  goToCompletedTask() {
    this.navCtrl.push(CompletedTaskPage);
  }

  gotoDoneTask(){
    this.navCtrl.push('DoneTaskPage');
  }

  presentActionSheet() {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Manage Time',
      buttons: [
        {
          text: 'Start Job',
          handler: () => {
            this.todaytime = moment().format();
            // alert("2");
            // console.log('Job Started');
            this.userServicesProvider.getTimingsByDate(this.userId, this.todayDate).then((response: any) => {
              // console.log("response",response[0].JobStartTime);
              if (response.length != 0) {
                if (response[0].JobStartTime == 'undefined') {
                  firebase.database().ref('XiraInfotech').child('UserProfile').child(this.userId).child('Timings').child(this.todayDate)
                    .update({
                      JobStartTime: this.todaytime,
                      key: this.todayDate
                    });
                    this.isToggled=true;
                  firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).
                    update({
                      userStatus: "online"
                    });
                } else {
                  alert("already added")
                }
              }
              else {
                firebase.database().ref('XiraInfotech').child('UserProfile').child(this.userId).child('Timings').child(this.todayDate)
                  .update({
                    JobStartTime: this.todaytime,
                    key: this.todayDate
                  });
                  this.isToggled=true;
                firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).
                  update({
                    userStatus: "online"
                  });
              }

            })


            // this.userServicesProvider.addJobStartTime(this.userId).then((res: any) => {
            //   res.update({
            //     JobStartDate: this.todayDate,
            //     JobStartTime: this.todaytime,
            //   })
            // })
          }
        }, {
          text: 'End Job',
          handler: () => {
            this.todaytime = moment().format();
            this.userServicesProvider.getTimingsByDate(this.userId, this.todayDate).then((response: any) => {
              console.log("response", response);
              if (response.length != 0) {
                if (response[0].JobEndTime == null) {
                  if(response[0].JobStartTime!=0){
                    firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).child('Timings').child(this.todayDate)
                    .update({
                      JobEndTime: this.todaytime
                    });
                    this.isToggled=false;
                  firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).
                    update({
                      userStatus: "offline",
                    });
                  }else{
                    alert("you have not started your job");
                  }
                 
                } else {
                  alert("already added")
                }
              }
              else {
                firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).child('Timings').child(this.todayDate)
                  .update({
                    JobEndTime: this.todaytime
                  });
                  this.isToggled=false;
                firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).
                  update({
                    userStatus: "offline",
                  });
              }

            })

            // console.log('Job Ended ');
            // firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).child('Timings').child(this.todayDate)
            //   .update({
            //     JobEndTime: this.todaytime
            //   });

            // firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).
            //   update({
            //     userStatus: "offline",
            //   });

            // this.userServicesProvider.addJobEndTime(this.userId, this.todayDate).then((response: any) => {
            //   response.update({
            //     JobEndDate: this.todayDate,
            //     JobEndTime: this.todaytime
            //   })
            // })
          }
        }, {
          cssClass:'yellow-color',
          text: 'Lunch start',
          handler: () => {
            // console.log('Lunch start ');
            this.todaytime = moment().format();
            this.userServicesProvider.getTimingsByDate(this.userId, this.todayDate).then((response: any) => {
              console.log("response", response);
              if (response.length != 0) {
                if (response[0].LunchStartTime == null) {
                  firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).child('Timings').child(this.todayDate)
                    .update({
                      LunchStartTime: this.todaytime
                    });
                  firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).
                    update({
                      userStatus: "pause",
                    });
                } else {
                  alert("already added")
                }
              }
              else {
                firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).child('Timings').child(this.todayDate)
                  .update({
                    LunchStartTime: this.todaytime
                  });
                firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).
                  update({
                    userStatus: "pause",
                  });
              }

            })



            // firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).child('Timings').child(this.todayDate)
            //   .update({
            //     LunchStartTime:this.todaytime
            //   });
            //   firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).
            //   update({
            //     userStatus: "pause",
            // });
            // this.userServicesProvider.addJobLunchStartTime(this.userId).then((response:any)=>{
            //   response.set({
            //     LunchDate:this.todayDate,
            //     LunchStartTime:this.todaytime
            //   })
            // })
          }
        }, {
          text: 'Lunch End',
          handler: () => {
            this.todaytime = moment().format();
            // console.log('Job Ended ');
            this.userServicesProvider.getTimingsByDate(this.userId, this.todayDate).then((response: any) => {
              console.log("response", response);
              if (response.length != 0) {
                if (response[0].LunchEndTime == null) {
                  if(response[0].LunchStartTime!=null){
                    firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).child('Timings').child(this.todayDate)
                    .update({
                      LunchEndTime: this.todaytime
                    });
                  firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).
                    update({
                      userStatus: "online",
                    });
                  }else{
                    alert("you have not started lunch yet")
                  }
                 
                } else {
                  alert("already added")
                }
              }
              else {
                firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).child('Timings').child(this.todayDate)
                  .update({
                    LunchEndTime: this.todaytime
                  });
                firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).
                  update({
                    userStatus: "online",
                  });
              }

            })

            // firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).child('Timings').child(this.todayDate)
            //   .update({
            //     LunchEndTime: this.todaytime
            //   });
            // firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).
            //   update({
            //     userStatus: "online",
            //   });

            // this.userServicesProvider.addJobLunchEndTime(this.userId).then((response: any) => {
            //   response.set({
            //     LunchDate: this.todayDate,
            //     LunchEndTime: this.todaytime
            //   })
            // })
          }
        }, {
          text: 'Extra Break Start',
          handler: () => {
            this.todaytime = moment().format();
            this.userServicesProvider.getTimingsByDate(this.userId, this.todayDate).then((response: any) => {
              console.log("response", response);
              if (response.length != 0) {
                if (response[0].ExtraBreakStart == null) {
                  firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).child('Timings').child(this.todayDate)
                    .update({
                      ExtraBreakStart: this.todaytime,
                    });

                  firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).
                    update({
                      userStatus: "pause",
                    });
                } else {
                  alert("already added")
                }
              }
              else {
                firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).child('Timings').child(this.todayDate)
                  .update({
                    ExtraBreakStart: this.todaytime,
                  });

                firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).
                  update({
                    userStatus: "pause",
                  });
              }

            })


            // console.log('Job Ended ');
            // firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).child('Timings').child(this.todayDate)
            //   .update({
            //     ExtraBreakStart: this.todaytime,
            //   });

            // firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).
            //   update({
            //     userStatus: "pause",
            //   });
            // this.userServicesProvider.addJobLunchEndTime(this.userId).then((response: any) => {
            //   response.set({
            //     LunchDate: this.todayDate,
            //     LunchEndTime: this.todaytime
            //   })
            // })
          }
        },
        {
          text: 'Extra Break End',
          handler: () => {
            this.todaytime = moment().format();
            this.userServicesProvider.getTimingsByDate(this.userId, this.todayDate).then((response: any) => {
              console.log("response", response);
              if (response.length != 0) {
                if (response[0].ExtraBreakEnd == null) {
                  if(response[0].ExtraBreakStart!=null){
                    firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).child('Timings').child(this.todayDate)
                    .update({
                      ExtraBreakEnd: this.todaytime
                    });

                  firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).
                    update({
                      userStatus: "online",
                    });
                  }else{
                    alert("you have not started you extra break")
                  }
                 
                } else {
                  alert("already added")
                }
              }
              else {
                firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).child('Timings').child(this.todayDate)
                  .update({
                    ExtraBreakEnd: this.todaytime
                  });

                firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).
                  update({
                    userStatus: "online",
                  });
              }

            })
            // console.log('Job Ended ');
            // firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).child('Timings').child(this.todayDate)
            //   .update({
            //     ExtraBreakEnd: this.todaytime
            //   });

            // firebase.database().ref(this.companyName).child('UserProfile').child(this.userId).
            //   update({
            //     userStatus: "online",
            //   });
            // this.userServicesProvider.addJobLunchEndTime(this.userId).then((response: any) => {
            //   response.set({
            //     LunchDate: this.todayDate,
            //     LunchEndTime: this.todaytime
            //   })
            // })
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            // console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

}
