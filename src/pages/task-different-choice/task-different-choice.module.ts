import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TaskDifferentChoicePage } from './task-different-choice';

@NgModule({
  declarations: [
    TaskDifferentChoicePage,
  ],
  imports: [
    IonicPageModule.forChild(TaskDifferentChoicePage),
  ],
  exports:[
    TaskDifferentChoicePage,
  ]
})
export class TaskDifferentChoicePageModule {}
