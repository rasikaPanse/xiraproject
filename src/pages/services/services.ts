import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import firebase from 'firebase';
import {AccordionModule} from 'primeng/accordion';

import { UserServicesProvider } from '../../providers/user-service/user-services'

/**
 * Generated class for the ServicesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-services',
  templateUrl: 'services.html',
})
export class ServicesPage {
  public ServiceList:any;
  itemExpandHeight: number = 100;

  constructor(public navCtrl: NavController, public navParams: NavParams,public userServicesProvider:UserServicesProvider) {
    this.userServicesProvider.getService().then((response:any)=>{
      console.log("respi",response);
      this.ServiceList=response
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ServicesPage');
  }
  getSeviceDetail(item){
    this.navCtrl.push('ServiceEditPage',{items:item});
    // console.log("item",item);
    // item.show=true;
      
  }
  doRefresh(refresher){
    console.log('begin sycn operaytion',refresher);
    this.ionViewDidLoad();
    setTimeout(()=>{
      console.log('Async operation has ended');
      refresher.complete();
    },2000);
  }

  addService(){
    this.navCtrl.push('AddProjectCategoryPage');
  }

}
