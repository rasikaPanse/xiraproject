import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AssignTaskPage } from './assign-task';

@NgModule({
  declarations: [
    AssignTaskPage,
  ],
  imports: [
    IonicPageModule.forChild(AssignTaskPage),
  ],
  exports:[
    AssignTaskPage
  ]
})
export class AssignTaskPageModule {}
