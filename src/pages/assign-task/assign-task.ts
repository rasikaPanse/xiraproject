import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import * as moment from 'moment';
import firebase from 'firebase';
import { UserServicesProvider } from '../../providers/user-service/user-services';
import { AdminMainPage } from '../admin-main/admin-main';


/**
 * Generated class for the AssignTaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-assign-task',
  templateUrl: 'assign-task.html',
})
export class AssignTaskPage {
  CurrentTime:any;
  projectName:any;
  taskName:any;
  userId:any;
  companyName:any;
  taskDescription:any;
  userProject:any;
  userDetails:any;
  projectList:any;
  Priority:any;
  public catadd:any=[];
  public todayDate = moment().format("MM-DD-YYYY");
  constructor(public toastCtrl:ToastController ,public userServicesProvider:UserServicesProvider,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad AssignTaskPage');
    this.userId=localStorage.getItem('userId');
    this.companyName='XiraInfotech'
    this.userDetails=this.navParams.get('userDetails');
    // console.log("this",this.userDetails);
    this.userServicesProvider.getProjectforUser().then((projectlistfromdb:any)=>{
      // console.log("helo",projectlistfromdb);
      this.projectList=projectlistfromdb;

    })
    // this.userServicesProvider.getAssignedProjects(this.userDetails.key).then((Response:any)=>{
    //   console.log("Response",Response);
    //   this.userProject= Response;
    // })
  }

  AddTask(){
    // alert("add");
    this.CurrentTime= moment().format("HH:mm");
    // console.log("projectName",this.projectName);
    // console.log("taskName",this.taskName);
    // console.log("this.userId",this.userId);
    // console.log("todayDate",this.todayDate);
    // console.log("CurrentTime",this.CurrentTime);
    // this.userServicesProvider.getTaskbyTaskStatus(this.userDetails.key,"started").then((response:any)=>{
    //   console.log("response1",response[0]);
      
    //   let diff = moment(this.CurrentTime, 'HH:mm').diff(moment(response[0].startTime, 'HH:mm'));
    //   console.log("diff",diff);
    //   let d = moment.duration(diff);
    //   console.log("d",d);
    //   let m= Math.floor(d.asHours()) + moment.utc(diff).format(":mm");
    //   console.log("m",m);
    //   // response[0].update({taskStatus:"pause"});  
    //   // firebase.database().ref(this.companyName).child('UserProfile').child(this.userDetails.key).child('Task').child(response[0].key)
    //   //   .update({
    //   //     taskStatus:"pause",
    //   //     timeTaken:m,
    //   //     endTime:this.CurrentTime
    //   //   });
    // }).catch((error:any)=> console.log(error));
          // console.log("response1",this.userDetails.key);

    this.userServicesProvider.addTaskToDb(this.userDetails.key).then((ress:any)=>{
      ress.set({
        assignedBy:"Admin",
        ProjectName: this.projectName,
        assignedDate:this.todayDate,
        TaskName:this.taskName,
        TaskDescription:this.taskDescription,
        assignedTime:this.CurrentTime,
        Priority:this.Priority,
        taskStatus:"given",
        userId:this.userDetails.key
      });
    })

    this.userServicesProvider.assignProjectToUser(this.userDetails.key).then((res:any)=>{
      res.set({
        projectName:this.projectName,
        key:res.key
      })
      
  })

  this.presentToast();
   this.navCtrl.setRoot(AdminMainPage)
  //  this.navCtrl.push(TabsPage,{index: "2"});
  }
  
  
  getProject(two) {
    console.log("ionchange",two);
    this.catadd=[];
    this.userServicesProvider.getcategoryByProjectName(two).then((resp:any)=>{
      console.log("resp",resp);
      for(let i=0;i<resp[0].projectDescription.length;i++){
              let respo = resp[0].projectDescription[i];
              this.catadd.push(resp[0].projectDescription[i]);
    
              // respo.map((resp:any)=>{
              //   // alert("3");
              //   console.log("respnse.......",resp);
              //   // response.userId=res[i].key;
              //   // resp.username=response[j].firstName;
              //   // this.projectAdd()
              //   console.log("projectTask123",this.projectAdd);
      
              //  //  this.completedTask.push({userId:res[i].key})
              // })
            }
    })
    // console.log('project',this.project);
    // for(let j=0;j<two.length;j++){
    //   // thi
    //   this.userServicesProvider.getcategoryByValue(two[j]).then((response: any) => {
    //     console.log("response", response[0].projectDescription.length);
    //     for(let i=0;i<response[0].projectDescription.length;i++){
    //       let respo = response[0].projectDescription[i];
    //       this.catadd.push(response[0].projectDescription[i]);

    //       // respo.map((resp:any)=>{
    //       //   // alert("3");
    //       //   console.log("respnse.......",resp);
    //       //   // response.userId=res[i].key;
    //       //   // resp.username=response[j].firstName;
    //       //   // this.projectAdd()
    //       //   console.log("projectTask123",this.projectAdd);
  
    //       //  //  this.completedTask.push({userId:res[i].key})
    //       // })
    //     }
      
    //   })
    // }
    setTimeout(() => {
      console.log("jbfjsd",this.catadd);
    // this.showCheckbox(this.project, this.projectAdd,two);

    }, 1000);

    // two is available here
    // but i need the value from first select (one) here as well 
  }

  presentToast() {
    const toast = this.toastCtrl.create({
      message: 'Task assigned successfully',
      duration: 3000
    });
    toast.present();
  }

}
