import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserServicesProvider } from '../../providers/user-service/user-services';
import firebase from 'firebase';
/**
 * Generated class for the UserSecondPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-second',
  templateUrl: 'user-second.html',
})
export class UserSecondPage {
  public companyName:any;
  public userDetails:any;
  public deleteUser:any
  constructor(public navCtrl: NavController, public navParams: NavParams,public userServicesProvider:UserServicesProvider) {
    // alert("3");
    
    this.deleteUser=firebase.database().ref('XiraInfotech').child('UserProfile')
    this.userServicesProvider.getUserOnAdminSide().then((userResponse:any)=>{
      // console.log("userResponse",userResponse);
      // this.companyName=localStorage.getItem('companyName');
      this.companyName='XiraInfotech';
      this.userDetails=userResponse;
      console.log("userDetails",this.userDetails);
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserSecondPage');
  }
  ionVieWillEnter(){
    
  }

  getUserTaskDetails(task){
    // console.log("task",task);
    this.navCtrl.push('UserprofilePage',{userdetails:task});
  }

  delete(user){
    console.log("user",user);
    this.deleteUser.child(user.key).remove();
    this.ionViewDidLoad();
  }

}
