import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminRegistrationPage } from './admin-registration';

@NgModule({
  declarations: [
    AdminRegistrationPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminRegistrationPage),
  ],
  exports:[
    AdminRegistrationPage
  ]
})
export class AdminRegistrationPageModule {}
