import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import firebase from 'firebase'
import {  FormGroup, Validators } from '@angular/forms';
import { FormBuilder} from '@angular/forms';
import { UserServicesProvider } from '../../providers/user-service/user-services';
/**
 * Generated class for the AdminRegistrationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-registration',
  templateUrl: 'admin-registration.html',
})
export class AdminRegistrationPage {
  registrationFormUser: FormGroup;
  isenabled:any;
  userProfile:any;
  errorMess:any;
  data = {
    
    UserEmail:"",
    company:"",
    password: "",
  
  };  
  constructor(public formBuilder:FormBuilder,public navCtrl: NavController, public navParams: NavParams,public userServicesProvider:UserServicesProvider) {
    this.userProfile = firebase.database();
    // this.desigArray =
   
    this.registrationFormUser = this.formBuilder.group({
      email: ['',Validators.compose([
        Validators.pattern(regexValidators.email),
        Validators.required
      ])],
      password: ['',Validators.compose([
        Validators.pattern(regexValidators.password),
        Validators.required
      ])
       
      ],
      phoneNo:['',Validators.compose([
        Validators.pattern(regexValidators.phoneNo),
        Validators.required
      ])
    ],
      company:['',Validators.required]
    });
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad AdminRegistrationPage');
  }
  registerAdmin(data){
    let email=data.UserEmail,
    password=data.password;
    this.userServicesProvider.registerWithEmailAndPassword(email,password).then((response:any)=>{
      // console.log("response",response);
      if(response!==null){
        localStorage.setItem("userId", response.user.uid);
        this.isenabled = false;

        if(response.operationType== 'signIn'){
          // console.log("dob",data.dob);
          // console.log("phoneno",data.designation);
          // localStorage.setItem("user", response.operationType);
          // localStorage.setItem("type","user");
          // localStorage.setItem("userEmail", response.user.email);
          // this.userProfile = firebase.database().ref(this.companyName).child('UserProfile').child(userId);

          firebase.database().ref(data.company).child('Admin').child(response.user.uid).set({
            email: data.UserEmail,
           
            company:data.company,
  
    
          });

          this.navCtrl.setRoot('AdminLoginPage');
          
        }     

      }
     
    },error=>{
      console.log("error fgdrgregistr",error.message);
      this.isenabled=true;
      this.errorMess=error.message;
    })
  }

}
const PURE_EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

// Passwords should be at least 8 characters long and should contain one number, one character and one special character.
const PASSWORD_REGEXP = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;

const PHONENUMBER =  /^\(?(\d{3})(\d{3})(\d{4})$/;
export const regexValidators = {
  email: PURE_EMAIL_REGEXP,
  password: PASSWORD_REGEXP,
  phoneNo: PHONENUMBER
};