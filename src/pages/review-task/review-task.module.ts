import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReviewTaskPage } from './review-task';

@NgModule({
  declarations: [
    ReviewTaskPage,
  ],
  imports: [
    IonicPageModule.forChild(ReviewTaskPage),
  ],
  exports:[
    ReviewTaskPage
  ]
})
export class ReviewTaskPageModule {}
