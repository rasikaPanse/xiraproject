import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,MenuController ,AlertController} from 'ionic-angular';
import { UserServicesProvider } from '../../providers/user-service/user-services'; 
import  firebase from 'firebase';
import * as moment from 'moment';
/**
 * Generated class for the ReviewTaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-review-task',
  templateUrl: 'review-task.html',
})
export class ReviewTaskPage {
  public userId:any;
  public completedTask:any=[];
  companyName:any;
  constructor(public menuCtrl: MenuController,public alertCtrl:AlertController,public navCtrl: NavController, public navParams: NavParams,public userServicesProvider:UserServicesProvider) {
    this.userId=localStorage.getItem('userId');  
    this.companyName= 'XiraInfotech';
    this.userServicesProvider.getUserOnAdminSide().then((res:any)=>{
      console.log("response",res);
      for(let i=0;i<res.length;i++){
        console.log("res",res[i]);
        this.userServicesProvider.getTaskbyTaskStatus(res[i].key,'completed').then((result:any)=>{
          console.log("result",result);
         result.map((response:any)=>{
           console.log("respnse.......",response);
           response.userId=res[i].key;
           response.firstName=res[i].firstName;
           this.completedTask.push(response);
          //  this.completedTask.push({userId:res[i].key})
           console.log("completedTask123",this.completedTask);
         })

          
        })
        
      }
     
    })
  }

  ionViewDidLoad() {
    // alert("n2d");

   
  }
  ionViewWillEnter(){
    // console.log("")
    console.log('ionViewDidLoad ReviewTaskPage');
    
  }

  done(task){
    console.log("task done",task);
    firebase.database().ref('XiraInfotech').child('UserProfile').child(this.userId).child('Task').child(task.key)
        .update({
          taskStatus: "done",
        });

  }

  pending(task){
    console.log("task",task);
    this.showPrompt(task);
  }

  showPrompt(task) {
    const prompt = this.alertCtrl.create({
      title: 'Remark',
      message: "Please Enter Remark",
      inputs: [
        {
          name: 'Remark',
          placeholder: 'Remark'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: data => {
            console.log('Saved clicked', data);
            // this.CurrentTime = moment().format("HH:mm");
            // console.log("CurrentTime", taskkey);

            // let diff = moment(this.CurrentTime, 'HH:mm').diff(moment(taskkey.startTime, 'HH:mm'));
            // console.log("diff", diff);
            // let d = moment.duration(diff);
            // console.log("d", d);
            // let m = Math.floor(d.asHours()) + moment.utc(diff).format(":mm");
            // console.log("m", m);
            console.log("companyName",this.companyName)
            firebase.database().ref('XiraInfotech').child('UserProfile').child(task.userId).child('Task').child(task.key).update({
               
                taskStatus: "pending",
                RemarkByAdmin: data.Remark
              });
              this.ionViewWillEnter();
              this.ionViewDidLoad();
              console.log("kl");
            
          }
        }
      ]
    });
    
    prompt.present();
  }

  detailsTask(task,index) {
    // console.log("index home", index)
    this.navCtrl.push('DetailedTaskPage', { taskdetails: task, index: index,flag:"review" })
  }

  doRefresh(refresher){
    console.log('begin sycn operaytion',refresher);
    this.ionViewDidLoad();
    setTimeout(()=>{
      console.log('Async operation has ended');
      refresher.complete();
    },2000);
  }

}
