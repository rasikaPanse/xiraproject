import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserServicesProvider } from '../../providers/user-service/user-services';
import { ToastController } from 'ionic-angular';
/**
 * Generated class for the AddDesignationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-designation',
  templateUrl: 'add-designation.html',
})
export class AddDesignationPage {
  public designation;
  public responsibilty;
  constructor(public navCtrl: NavController, public navParams: NavParams,public toastCtrl: ToastController,public userServicesProvider:UserServicesProvider) {
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad AddDesignationPage');
  }

  addesignation(){
    // console.log("designation",this.designation);
    // console.log("responsibilty",this.responsibilty);
    this.userServicesProvider.getDesignationByValue(this.designation).then((res:any)=>{
      // console.log("res",res);
      if(res.length==0){
        this.userServicesProvider.addDesignationForUser().then((response:any)=>{
            // console.log("res")
            response.set({
              designation:this.designation,
              // responsibility:this.responsibilty
            })
            this.presentToastfornew(this.designation);
            this.navCtrl.setRoot('AdminMainPage');
          })
      }
      else{ 
        this.presentToast(this.designation);
      }
    })
  
    
    // addDesignationForUser
  }

  presentToast( desig) {
    const toast = this.toastCtrl.create({
      message: 'Designation  '+  desig +' already there',
      duration: 3000
    });
    toast.present();
  }

  presentToastfornew(desig) {
    const toast = this.toastCtrl.create({
      message: 'Designation'+ desig +' added successfully',
      duration: 3000
    });
    toast.present();
  }


}
