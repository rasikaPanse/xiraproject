import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddDesignationPage } from './add-designation';

@NgModule({
  declarations: [
    AddDesignationPage,
  ],
  imports: [
    IonicPageModule.forChild(AddDesignationPage),
  ],
  exports:[
    AddDesignationPage,
  ]
})
export class AddDesignationPageModule {}
