import { NgModule } from '@angular/core';
import { TimeformatePipe } from './timeformate/timeformate';
@NgModule({
	declarations: [TimeformatePipe],
	imports: [],
	exports: [TimeformatePipe]
})
export class PipesModule {}
